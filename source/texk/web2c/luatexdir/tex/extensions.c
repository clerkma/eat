#include "ptexlib.h"


alpha_file write_file[16];
halfword write_file_mode[16];
halfword write_file_translation[16];
boolean write_open[18];
scaled neg_wd;
scaled pos_wd;
scaled neg_ht;
halfword write_loc;

void do_extension(PDF pdf)
{
    int i,k;
    halfword p;
    switch(cur_chr){
        case open_node:

            new_write_whatsit(open_node_size);
            scan_optional_equals();
            scan_file_name();
            open_name(tail)= cur_name;
            open_area(tail)= cur_area;
            open_ext(tail)= cur_ext;
            break;
        case write_node:
            k= cur_cs;
            new_write_whatsit(write_node_size);
            cur_cs= k;
            p= scan_toks(false,false);
            write_tokens(tail)= def_ref;
            break;
        case close_node:
            new_write_whatsit(write_node_size);
            write_tokens(tail)= null;
            break;
        case special_node:
            new_whatsit(special_node);
            write_stream(tail)= null;
            p= scan_toks(false,true);
            write_tokens(tail)= def_ref;
            break;
        case immediate_code:
            get_x_token();
            if(cur_cmd==extension_cmd){
                if(cur_chr<=close_node){
                    p= tail;
                    do_extension(pdf);
                    out_what(pdf,tail);
                    flush_node_list(tail);
                    tail= p;
                    vlink(p)= null;
                }else{
                    switch(cur_chr){
                        case pdf_obj_code:
                            check_o_mode(pdf,"\\immediate\\pdfobj",1<<OMODE_PDF,
                                    true);
                            do_extension(pdf);
                            if(obj_data_ptr(pdf,pdf_last_obj)==0)
                                pdf_error("ext1",
                                        "`\\pdfobj reserveobjnum' cannot be used with \\immediate");
                            pdf_write_obj(pdf,pdf_last_obj);
                            break;
                        case pdf_xform_code:
                            check_o_mode(pdf,"\\immediate\\pdfxform",1<<OMODE_PDF,
                                    true);
                            do_extension(pdf);
                            pdf_cur_form= pdf_last_xform;
                            ship_out(pdf,obj_xform_box(pdf,pdf_last_xform),SHIPPING_FORM);
                            break;
                        case pdf_ximage_code:
                            check_o_mode(pdf,"\\immediate\\pdfximage",1<<OMODE_PDF,
                                    true);
                            do_extension(pdf);
                            pdf_write_image(pdf,pdf_last_ximage);
                            break;
                        default:
                            back_input();
                            break;
                    }
                }
            }else{
                back_input();
            }
            break;
        case pdf_annot_node:
            check_o_mode(pdf,"\\pdfannot",1<<OMODE_PDF,false);
            scan_annot(pdf);
            break;
        case pdf_catalog_code:
            check_o_mode(pdf,"\\pdfcatalog",1<<OMODE_PDF,true);
            scan_pdfcatalog(pdf);
            break;
        case pdf_dest_node:
            check_o_mode(pdf,"\\pdfdest",1<<OMODE_PDF,false);
            scan_pdfdest(pdf);
            break;
        case pdf_end_link_node:
            check_o_mode(pdf,"\\pdfendlink",1<<OMODE_PDF,false);
            if(abs(mode)==vmode)
                pdf_error("ext1","\\pdfendlink cannot be used in vertical mode");
            new_whatsit(pdf_end_link_node);
            break;
        case pdf_end_thread_node:
            check_o_mode(pdf,"\\pdfendthread",1<<OMODE_PDF,false);
            new_whatsit(pdf_end_thread_node);
            break;
        case pdf_font_attr_code:
            check_o_mode(pdf,"\\pdffontattr",1<<OMODE_PDF,false);
            scan_font_ident();
            k= cur_val;
            if(k==null_font)
                pdf_error("font","invalid font identifier");
            scan_pdf_ext_toks();
            set_pdf_font_attr(k,tokens_to_string(def_ref));
            if(str_length(pdf_font_attr(k))==0){
                flush_str((str_ptr-1));
                set_pdf_font_attr(k,0);
            }
            break;
        case pdf_font_expand_code:
            read_expand_font();
            break;
        case pdf_include_chars_code:
            check_o_mode(pdf,"\\pdfincludechars",1<<OMODE_PDF,false);
            pdf_include_chars(pdf);
            break;
        case pdf_info_code:
            check_o_mode(pdf,"\\pdfinfo",1<<OMODE_PDF,false);
            scan_pdf_ext_toks();
            pdf_info_toks= concat_tokens(pdf_info_toks,def_ref);
            break;
        case pdf_literal_node:
            check_o_mode(pdf,"\\pdfliteral",1<<OMODE_PDF,false);
            new_whatsit(pdf_literal_node);
            if(scan_keyword("direct"))
                set_pdf_literal_mode(tail,direct_always);
            else if(scan_keyword("page"))
                set_pdf_literal_mode(tail,direct_page);
            else
                set_pdf_literal_mode(tail,set_origin);
            scan_pdf_ext_toks();
            set_pdf_literal_type(tail,normal);
            set_pdf_literal_data(tail,def_ref);
            break;
        case pdf_colorstack_node:
            check_o_mode(pdf,"\\pdfcolorstack",1<<OMODE_PDF,false);
            scan_int();
            if(cur_val>=colorstackused()){
                print_err("Unknown color stack number ");
                print_int(cur_val);
                help3
                    ("Allocate and initialize a color stack with \\pdfcolorstackinit.",
                     "I'll use default color stack 0 here.",
                     "Proceed, with fingers crossed.");
                error();
                cur_val= 0;
            }
            if(cur_val<0){
                print_err("Invalid negative color stack number");
                help2("I'll use default color stack 0 here.",
                        "Proceed, with fingers crossed.");
                error();
                cur_val= 0;
            }
            if(scan_keyword("set"))
                i= colorstack_set;
            else if(scan_keyword("push"))
                i= colorstack_push;
            else if(scan_keyword("pop"))
                i= colorstack_pop;
            else if(scan_keyword("current"))
                i= colorstack_current;
            else
                i= -1;
            if(i>=0){
                new_whatsit(pdf_colorstack_node);
                set_pdf_colorstack_stack(tail,cur_val);
                set_pdf_colorstack_cmd(tail,i);
                set_pdf_colorstack_data(tail,null);
                if(i<=colorstack_data){
                    scan_pdf_ext_toks();
                    set_pdf_colorstack_data(tail,def_ref);
                }
            }else{
                print_err("Color stack action is missing");
                help3("The expected actions for \\pdfcolorstack:",
                        "    set, push, pop, current",
                        "I'll ignore the color stack command.");
                error();
            }
            break;
        case pdf_setmatrix_node:
            check_o_mode(pdf,"\\pdfsetmatrix",1<<OMODE_PDF,false);
            new_whatsit(pdf_setmatrix_node);
            scan_pdf_ext_toks();
            set_pdf_setmatrix_data(tail,def_ref);
            break;
        case pdf_save_node:
            check_o_mode(pdf,"\\pdfsave",1<<OMODE_PDF,false);
            new_whatsit(pdf_save_node);
            break;
        case pdf_restore_node:
            check_o_mode(pdf,"\\pdfrestore",1<<OMODE_PDF,false);
            new_whatsit(pdf_restore_node);
            break;
        case pdf_map_file_code:
            check_o_mode(pdf,"\\pdfmapfile",1<<OMODE_PDF,false);
            scan_pdf_ext_toks();
            pdfmapfile(def_ref);
            delete_token_ref(def_ref);
            break;
        case pdf_map_line_code:
            check_o_mode(pdf,"\\pdfmapline",1<<OMODE_PDF,false);
            scan_pdf_ext_toks();
            pdfmapline(def_ref);
            delete_token_ref(def_ref);
            break;
        case pdf_names_code:
            check_o_mode(pdf,"\\pdfnames",1<<OMODE_PDF,false);
            scan_pdf_ext_toks();
            pdf_names_toks= concat_tokens(pdf_names_toks,def_ref);
            break;
        case pdf_obj_code:
            check_o_mode(pdf,"\\pdfobj",1<<OMODE_PDF,false);
            scan_obj(pdf);
            break;
        case pdf_outline_code:
            check_o_mode(pdf,"\\pdfoutline",1<<OMODE_PDF,true);
            scan_pdfoutline(pdf);
            break;
        case pdf_refobj_node:
            check_o_mode(pdf,"\\pdfrefobj",1<<OMODE_PDF,false);
            scan_refobj(pdf);
            break;
        case pdf_refxform_node:
            check_o_mode(pdf,"\\pdfrefxform",1<<OMODE_PDF,false);
            scan_pdfrefxform(pdf);
            break;
        case pdf_refximage_node:
            check_o_mode(pdf,"\\pdfrefximage",1<<OMODE_PDF,false);
            scan_pdfrefximage(pdf);
            break;
        case pdf_save_pos_node:
            new_whatsit(pdf_save_pos_node);
            break;
        case pdf_start_link_node:
            check_o_mode(pdf,"\\pdfstartlink",1<<OMODE_PDF,false);
            scan_startlink(pdf);
            break;
        case pdf_start_thread_node:
            check_o_mode(pdf,"\\pdfstartthread",1<<OMODE_PDF,false);
            new_annot_whatsit(pdf_start_thread_node);
            scan_thread_id();
            break;
        case pdf_thread_node:
            check_o_mode(pdf,"\\pdfthread",1<<OMODE_PDF,false);
            new_annot_whatsit(pdf_thread_node);
            scan_thread_id();
            break;
        case pdf_trailer_code:
            check_o_mode(pdf,"\\pdftrailer",1<<OMODE_PDF,false);
            scan_pdf_ext_toks();
            pdf_trailer_toks= concat_tokens(pdf_trailer_toks,def_ref);
            break;
        case pdf_xform_code:
            check_o_mode(pdf,"\\pdfxform",1<<OMODE_PDF,false);
            scan_pdfxform(pdf);
            break;
        case pdf_ximage_code:
            check_o_mode(pdf,"\\pdfximage",1<<OMODE_PDF,false);
            fix_pdf_minorversion(pdf);
            scan_pdfximage(pdf);
            break;
        case save_cat_code_table_code:
            scan_int();
            if((cur_val<0)||(cur_val> 0x7FFF)){
                print_err("Invalid \\catcode table");
                help1("All \\catcode table ids must be between 0 and 0x7FFF");
                error();
            }else{
                if(cur_val==cat_code_table){
                    print_err("Invalid \\catcode table");
                    help1("You cannot overwrite the current \\catcode table");
                    error();
                }else{
                    copy_cat_codes(cat_code_table,cur_val);
                }
            }
            break;
        case init_cat_code_table_code:
            scan_int();
            if((cur_val<0)||(cur_val> 0x7FFF)){
                print_err("Invalid \\catcode table");
                help1("All \\catcode table ids must be between 0 and 0x7FFF");
                error();
            }else{
                if(cur_val==cat_code_table){
                    print_err("Invalid \\catcode table");
                    help1("You cannot overwrite the current \\catcode table");
                    error();
                }else{
                    initex_cat_codes(cur_val);
                }
            }
            break;
        case set_random_seed_code:
            scan_int();
            if(cur_val<0)
                negate(cur_val);
            random_seed= cur_val;
            init_randoms(random_seed);
            break;
        case pdf_glyph_to_unicode_code:
            glyph_to_unicode();
            break;
        case late_lua_node:
            new_whatsit(late_lua_node);
            late_lua_name(tail)= scan_lua_state();
            (void)scan_toks(false,false);
            late_lua_data(tail)= def_ref;
            break;
        default:
            confusion("ext1");
            break;
    }
}

void new_whatsit(int s)
{
    halfword p;
    p= new_node(whatsit_node,s);
    couple_nodes(tail,p);
    tail= p;
}

void new_write_whatsit(int w)
{
    new_whatsit(cur_chr);
    if(w!=write_node_size){
        scan_four_bit_int();
    }else{
        scan_int();
        if(cur_val<0)
            cur_val= 17;
        else if((cur_val> 15)&&(cur_val!=18))
            cur_val= 16;
    }
    write_stream(tail)= cur_val;
}

void scan_pdf_ext_toks(void)
{
    (void)scan_toks(false,true);
}

halfword prev_rightmost(halfword s,halfword e)
{
    halfword p= s;
    if(p==null)
        return null;
    while(vlink(p)!=e){
        p= vlink(p);
        if(p==null)
            return null;
    }
    return p;
}

int pdf_last_xform;

int pdf_last_ximage;
int pdf_last_ximage_pages;
int pdf_last_ximage_colordepth;
int pdf_last_annot;
int pdf_last_link;
scaledpos pdf_last_pos= {0,0};

halfword concat_tokens(halfword q,halfword r)
{
    halfword p;
    if(q==null)
        return r;
    p= q;
    while(token_link(p)!=null)
        p= token_link(p);
    set_token_link(p,token_link(r));
    free_avail(r);
    return q;
}

int pdf_retval;

halfword make_local_par_node(void)
{
    halfword p,q;
    p= new_node(whatsit_node,local_par_node);
    local_pen_inter(p)= local_inter_line_penalty;
    local_pen_broken(p)= local_broken_penalty;
    if(local_left_box!=null){
        q= copy_node_list(local_left_box);
        local_box_left(p)= q;
        local_box_left_width(p)= width(local_left_box);
    }
    if(local_right_box!=null){
        q= copy_node_list(local_right_box);
        local_box_right(p)= q;
        local_box_right_width(p)= width(local_right_box);
    }
    local_par_dir(p)= par_direction;
    return p;
}

boolean*eof_seen;

void print_group(boolean e)
{
    switch(cur_group){
        case bottom_level:
            tprint("bottom level");
            return;
            break;
        case simple_group:
        case semi_simple_group:
            if(cur_group==semi_simple_group)
                tprint("semi ");
            tprint("simple");
            break;;
        case hbox_group:
        case adjusted_hbox_group:
            if(cur_group==adjusted_hbox_group)
                tprint("adjusted ");
            tprint("hbox");
            break;
        case vbox_group:
            tprint("vbox");
            break;
        case vtop_group:
            tprint("vtop");
            break;
        case align_group:
        case no_align_group:
            if(cur_group==no_align_group)
                tprint("no ");
            tprint("align");
            break;
        case output_group:
            tprint("output");
            break;
        case disc_group:
            tprint("disc");
            break;
        case insert_group:
            tprint("insert");
            break;
        case vcenter_group:
            tprint("vcenter");
            break;
        case math_group:
        case math_choice_group:
        case math_shift_group:
        case math_left_group:
            tprint("math");
            if(cur_group==math_choice_group)
                tprint(" choice");
            else if(cur_group==math_shift_group)
                tprint(" shift");
            else if(cur_group==math_left_group)
                tprint(" left");
            break;
    }
    tprint(" group (level ");
    print_int(cur_level);
    print_char(')');
    if(saved_value(-1)!=0){
        if(e)
            tprint(" entered at line ");
        else
            tprint(" at line ");
        print_int(saved_value(-1));
    }
}

void group_trace(boolean e)
{
    begin_diagnostic();
    print_char('{');
    if(e)
        tprint("leaving ");
    else
        tprint("entering ");
    print_group(e);
    print_char('}');
    end_diagnostic(false);
}

save_pointer*grp_stack;
halfword*if_stack;

void group_warning(void)
{
    int i;
    boolean w;
    base_ptr= input_ptr;
    input_stack[base_ptr]= cur_input;
    i= in_open;
    w= false;
    while((grp_stack[i]==cur_boundary)&&(i> 0)){
        if(tracing_nesting> 0){
            while((input_stack[base_ptr].state_field==token_list)||
                    (input_stack[base_ptr].index_field> i))
                decr(base_ptr);
            if(input_stack[base_ptr].name_field> 17)
                w= true;
        }
        grp_stack[i]= save_value(save_ptr);
        decr(i);
    }
    if(w){
        tprint_nl("Warning: end of ");
        print_group(true);
        tprint(" of a different file");
        print_ln();
        if(tracing_nesting> 1)
            show_context();
        if(history==spotless)
            history= warning_issued;
    }
}

void if_warning(void)
{
    int i;
    boolean w;
    base_ptr= input_ptr;
    input_stack[base_ptr]= cur_input;
    i= in_open;
    w= false;
    while(if_stack[i]==cond_ptr){
        if(tracing_nesting> 0){
            while((input_stack[base_ptr].state_field==token_list)||
                    (input_stack[base_ptr].index_field> i))
                decr(base_ptr);
            if(input_stack[base_ptr].name_field> 17)
                w= true;
        }
        if_stack[i]= vlink(cond_ptr);
        decr(i);
    }
    if(w){
        tprint_nl("Warning: end of ");
        print_cmd_chr(if_test_cmd,cur_if);
        print_if_line(if_line);
        tprint(" of a different file");
        print_ln();
        if(tracing_nesting> 1)
            show_context();
        if(history==spotless)
            history= warning_issued;
    }
}

void file_warning(void)
{
    halfword p;
    int l;
    int c;
    int i;
    p= save_ptr;
    l= cur_level;
    c= cur_group;
    save_ptr= cur_boundary;
    while(grp_stack[in_open]!=save_ptr){
        decr(cur_level);
        tprint_nl("Warning: end of file when ");
        print_group(true);
        tprint(" is incomplete");
        cur_group= save_level(save_ptr);
        save_ptr= save_value(save_ptr);
    }
    save_ptr= p;
    cur_level= (quarterword)l;
    cur_group= (group_code)c;
    p= cond_ptr;
    l= if_limit;
    c= cur_if;
    i= if_line;
    while(if_stack[in_open]!=cond_ptr){
        tprint_nl("Warning: end of file when ");
        print_cmd_chr(if_test_cmd,cur_if);
        if(if_limit==fi_code)
            tprint_esc("else");
        print_if_line(if_line);
        tprint(" is incomplete");
        if_line= if_line_field(cond_ptr);
        cur_if= if_limit_subtype(cond_ptr);
        if_limit= if_limit_type(cond_ptr);
        cond_ptr= vlink(cond_ptr);
    }
    cond_ptr= p;
    if_limit= l;
    cur_if= c;
    if_line= i;
    print_ln();
    if(tracing_nesting> 1)
        show_context();
    if(history==spotless)
        history= warning_issued;
}

halfword last_line_fill;


#define get_tex_dimen_register(j) dimen(j)
#define get_tex_skip_register(j) skip(j)
#define get_tex_count_register(j) count(j)
#define get_tex_attribute_register(j) attribute(j)
#define get_tex_box_register(j) box(j)

int set_tex_dimen_register(int j,scaled v)
{
    int a;
    if(global_defs> 0)
        a= 4;
    else
        a= 0;
    word_define(j+scaled_base,v);
    return 0;
}

int set_tex_skip_register(int j,halfword v)
{
    int a;
    if(global_defs> 0)
        a= 4;
    else
        a= 0;
    if(type(v)!=glue_spec_node)
        return 1;
    word_define(j+skip_base,v);
    return 0;
}

int set_tex_count_register(int j,scaled v)
{
    int a;
    if(global_defs> 0)
        a= 4;
    else
        a= 0;
    word_define(j+count_base,v);
    return 0;
}

int set_tex_box_register(int j,scaled v)
{
    int a;
    if(global_defs> 0)
        a= 4;
    else
        a= 0;
    define(j+box_base,box_ref_cmd,v);
    return 0;
}

int set_tex_attribute_register(int j,scaled v)
{
    int a;
    if(global_defs> 0)
        a= 4;
    else
        a= 0;
    if(j> max_used_attr)
        max_used_attr= j;
    attr_list_cache= cache_disabled;
    word_define(j+attribute_base,v);
    return 0;
}

int get_tex_toks_register(int j)
{
    str_number s;
    s= get_nullstr();
    if(toks(j)!=null){
        s= tokens_to_string(toks(j));
    }
    return s;
}

int set_tex_toks_register(int j,lstring s)
{
    halfword ref;
    int a;
    ref= get_avail();
    (void)str_toks(s);
    set_token_ref_count(ref,0);
    set_token_link(ref,token_link(temp_token_head));
    if(global_defs> 0)
        a= 4;
    else
        a= 0;
    define(j+toks_base,call_cmd,ref);
    return 0;
}

scaled get_tex_box_width(int j)
{
    halfword q= box(j);
    if(q!=null)
        return width(q);
    return 0;
}

int set_tex_box_width(int j,scaled v)
{
    halfword q= box(j);
    if(q==null)
        return 1;
    width(q)= v;
    return 0;
}

scaled get_tex_box_height(int j)
{
    halfword q= box(j);
    if(q!=null)
        return height(q);
    return 0;
}

int set_tex_box_height(int j,scaled v)
{
    halfword q= box(j);
    if(q==null)
        return 1;
    height(q)= v;
    return 0;
}


scaled get_tex_box_depth(int j)
{
    halfword q= box(j);
    if(q!=null)
        return depth(q);
    return 0;
}

int set_tex_box_depth(int j,scaled v)
{
    halfword q= box(j);
    if(q==null)
        return 1;
    depth(q)= v;
    return 0;
}

int synctexoption;
int synctexoffset;

pool_pointer edit_name_start;
int edit_name_length,edit_line;
int ipcon;
boolean stop_at_space;

int shellenabledp;
int restrictedshell;
char*output_comment;

boolean debug_format_file;/*:33*/
