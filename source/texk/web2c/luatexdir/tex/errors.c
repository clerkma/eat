#include "ptexlib.h"

int interaction;
int interactionoption;
const char*last_error;

void print_err(const char*s)
{
    if(interaction==error_stop_mode){
        wake_up_terminal();
    }
    if(filelineerrorstylep)
        print_file_line();
    else
        tprint_nl("! ");
    tprint(s);
    last_error= (const char*)s;
}

void fixup_selector(boolean logopened)
{
    if(interaction==batch_mode)
        selector= no_print;
    else
        selector= term_only;
    if(logopened)
        selector= selector+2;
}

boolean deletions_allowed;
boolean set_box_allowed;
int history;
int error_count;
int interrupt;
boolean OK_to_interrupt;

void initialize_errors(void)
{
    if(interactionoption==unspecified_mode)
        interaction= error_stop_mode;
    else
        interaction= interactionoption;
    deletions_allowed= true;
    set_box_allowed= true;
    OK_to_interrupt= true;
}

const char*help_line[7];
boolean use_err_help;

__attribute__((noreturn))
    void do_final_end(void)
{
    update_terminal();
    ready_already= 0;
    if((history!=spotless)&&(history!=warning_issued))
        uexit(1);
    else
        uexit(0);
}

__attribute__((noreturn))
    void jump_out(void)
{
    close_files_and_terminate();
    do_final_end();
}

void error(void)
{
    ASCII_code c;
    int callback_id;
    int s1,s2,s3,s4;
    int i;
    if(history<error_message_issued)
        history= error_message_issued;
    print_char('.');
    callback_id= callback_defined(show_error_hook_callback);
    if(callback_id> 0)
        run_callback(callback_id,"->");
    show_context();
    if(haltonerrorp){
        history= fatal_error_stop;
        jump_out();
    }
    if(interaction==error_stop_mode){

        while(1){
CONTINUE:
            clear_for_error_prompt();
            prompt_input("? ");
            if(last==first)
                return;
            c= buffer[first];
            if(c>='a')
                c= c+'A'-'a';
            switch(c){
            case'0':
            case'1':
            case'2':
            case'3':
            case'4':
            case'5':
            case'6':
            case'7':
            case'8':
            case'9':
                if(deletions_allowed){
                    s1= cur_tok;
                    s2= cur_cmd;
                    s3= cur_chr;
                    s4= align_state;
                    align_state= 1000000;
                    OK_to_interrupt= false;
                    if((last> first+1)&&(buffer[first+1]>='0')
                        &&(buffer[first+1]<='9'))
                        c= c*10+buffer[first+1]-'0'*11;
                    else
                        c= c-'0';
                    while(c> 0){
                        get_token();
                        decr(c);
                    }
                    cur_tok= s1;
                    cur_cmd= s2;
                    cur_chr= s3;
                    align_state= s4;
                    OK_to_interrupt= true;
                    help2("I have just deleted some text, as you asked.",
                        "You can now delete more, or insert, or whatever.");
                    show_context();
                    goto CONTINUE;
                }
                break;
#ifdef DEBUG
            case'D':
                debug_help();
                goto CONTINUE;
                break;
#endif
            case'E':
                if(base_ptr> 0){
                    tprint_nl("You want to edit file ");
                    print(input_stack[base_ptr].name_field);
                    tprint(" at line ");
                    print_int(line);
                    interaction= scroll_mode;
                    jump_out();
                }
                break;
            case'H':

                if(use_err_help){
                    give_err_help();
                    use_err_help= false;
                }else{
                    if(help_line[0]==NULL){
                        help2
                            ("Sorry, I don't know how to help in this situation.",
                            "Maybe you should try asking a human?");
                    }
                    i= 0;
                    while(help_line[i]!=NULL)
                        tprint_nl(help_line[i++]);
                    help4("Sorry, I already gave what help I could...",
                        "Maybe you should try asking a human?",
                        "An error might have occurred before I noticed any problems.",
                        "``If all else fails, read the instructions.''");
                    goto CONTINUE;
                }
                break;
            case'I':
                begin_file_reading();
                if(last> first+1){
                    iloc= first+1;
                    buffer[first]= ' ';
                }else{
                    prompt_input("insert>");
                    iloc= first;
                }
                first= last;
                ilimit= last-1;
                return;
                break;
            case'Q':
            case'R':
            case'S':
                error_count= 0;
                interaction= batch_mode+c-'Q';
                tprint("OK, entering ");
                switch(c){
                case'Q':
                    tprint_esc("batchmode");
                    decr(selector);
                    break;
                case'R':
                    tprint_esc("nonstopmode");
                    break;
                case'S':
                    tprint_esc("scrollmode");
                    break;
                }
                tprint("...");
                print_ln();
                update_terminal();
                return;
                break;
            case'X':
                interaction= scroll_mode;
                jump_out();
                break;
            default:
                break;
            }

            tprint
                ("Type <return> to proceed, S to scroll future error messages,");
            tprint_nl("R to run without stopping, Q to run quietly,");
            tprint_nl("I to insert something, ");
            if(base_ptr> 0)
                tprint("E to edit your file,");
            if(deletions_allowed)
                tprint_nl
                ("1 or ... or 9 to ignore the next 1 to 9 tokens of input,");
            tprint_nl("H for help, X to quit.");
        }

    }
    incr(error_count);
    if(error_count==100){
        tprint_nl("(That makes 100 errors; please try again.)");
        history= fatal_error_stop;
        jump_out();
    }

    if(interaction> batch_mode)
        decr(selector);
    if(use_err_help){
        print_ln();
        give_err_help();
    }else{
        int i1= 0;
        while(help_line[i1]!=NULL)
            tprint_nl(help_line[i1++]);
    }
    print_ln();
    if(interaction> batch_mode)
        incr(selector);
    print_ln();
}

void int_error(int n)
{
    tprint(" (");
    print_int(n);
    print_char(')');
    error();
}

void normalize_selector(void)
{
    if(log_opened_global)
        selector= term_and_log;
    else
        selector= term_only;
    if(job_name==0)
        open_log_file();
    if(interaction==batch_mode)
        decr(selector);
}

void succumb(void)
{
    if(interaction==error_stop_mode)
        interaction= scroll_mode;
    if(log_opened_global)
        error();
#ifdef DEBUG
    if(interaction> batch_mode)
        debug_help();
#endif
    history= fatal_error_stop;
    jump_out();
}

void fatal_error(const char*s)
{
    normalize_selector();
    print_err("Emergency stop");
    help1(s);
    succumb();
}

void lua_norm_error(const char*s)
{
    int saved_new_line_char;
    saved_new_line_char= new_line_char;
    new_line_char= 10;
    print_err("LuaTeX error ");
    tprint(s);
    help2("The lua interpreter ran into a problem, so the",
        "remainder of this lua chunk will be ignored.");
    error();
    new_line_char= saved_new_line_char;
}

void lua_fatal_error(const char*s)
{
    new_line_char= 10;
    normalize_selector();
    print_err("LuaTeX fatal error ");
    tprint(s);
    succumb();
}

void overflow(const char*s,unsigned int n)
{
    normalize_selector();
    print_err("TeX capacity exceeded, sorry [");
    tprint(s);
    print_char('=');
    print_int((int)n);
    print_char(']');
    help2("If you really absolutely need more capacity,",
        "you can ask a wizard to enlarge me.");
    succumb();
}

void confusion(const char*s)
{
    normalize_selector();
    if(history<error_message_issued){
        print_err("This can't happen (");
        tprint(s);
        print_char(')');
        help1("I'm broken. Please show this to someone who can fix can fix");
    }else{
        print_err("I can't go on meeting you like this");
        help2("One of your faux pas seems to have wounded me deeply...",
            "in fact, I'm barely conscious. Please fix it and try again.");
    }
    succumb();
}

void check_interrupt(void)
{
    if(interrupt!=0)
        pause_for_instructions();
}

void pause_for_instructions(void)
{
    if(OK_to_interrupt){
        interaction= error_stop_mode;
        if((selector==log_only)||(selector==no_print))
            incr(selector);
        print_err("Interruption");
        help3("You rang?",
            "Try to insert some instructions for me (e.g.,`I\\showlists'),",
            "unless you just want to quit by typing `X'.");
        deletions_allowed= false;
        error();
        deletions_allowed= true;
        interrupt= 0;
    }
}

void tex_error(const char*msg,const char**hlp)
{
    print_err(msg);
    if(hlp!=NULL){
        int i;
        for(i= 0;(hlp[i]!=NULL&&i<=5);i++){
            help_line[i]= hlp[i];
        }
        help_line[i]= NULL;
    }else{
        help_line[0]= NULL;
    }
    error();
}

void back_error(void)
{
    OK_to_interrupt= false;
    back_input();
    OK_to_interrupt= true;
    error();
}

void ins_error(void)
{
    OK_to_interrupt= false;
    back_input();
    token_type= inserted;
    OK_to_interrupt= true;
    error();
}

void char_warning(internal_font_number f,int c)
{
    int old_setting;
    int k;
    if(int_par(tracing_lost_chars_code)> 0){
        old_setting= int_par(tracing_online_code);
        if(int_par(tracing_lost_chars_code)> 1)
            int_par(tracing_online_code)= 1;
        begin_diagnostic();
        tprint_nl("Missing character: There is no ");
        print(c);
        tprint(" (U+");
        k= 0;
        if(c<16)
            print_char('0');
        if(c<256)
            print_char('0');
        if(c<4096)
            print_char('0');
        do{
            dig[k]= c%16;
            c= c/16;
            incr(k);
        }while(c!=0);
        print_the_digs((eight_bits)k);
        tprint(") in font ");
        print_font_name(f);
        print_char('!');
        end_diagnostic(false);
        int_par(tracing_online_code)= old_setting;
    }
}
