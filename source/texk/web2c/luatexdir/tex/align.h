#ifndef ALIGN_H
#define ALIGN_H 1

#define tab_mark_cmd_code 1114113   /*  {|biggest_char+2|} */
#define span_code 1114114           /*  {|biggest_char+3|} */
#define cr_code (span_code+1)       /* distinct from |span_code| and from any character */
#define cr_cr_code (cr_code+1)      /* this distinguishes \.{\\crcr} from \.{\\cr} */

extern void init_align(void);
extern void initialize_alignments(void);

extern boolean fin_col(void);
extern void fin_row(void);

extern void align_peek(void);
extern void insert_vj_template(void);

#endif
