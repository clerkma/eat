#ifndef FILENAME_H
#  define FILENAME_H

extern void scan_file_name(void);
extern void scan_file_name_toks(void);
extern char *pack_job_name(const char *s);
extern char *prompt_file_name(const char *s, const char *e);
extern void tprint_file_name(unsigned char *n, unsigned char *a,
                             unsigned char *e);
extern void print_file_name(str_number, str_number, str_number);

#endif
