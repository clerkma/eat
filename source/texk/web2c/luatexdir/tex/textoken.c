/*1:*/
#line 20 "./textoken.w"

static const char _svn_version[]= 
"$Id: textoken.w 4586 2013-03-01 09:52:21Z taco $"
"$URL: https://foundry.supelec.fr/svn/luatex/trunk/source/texk/web2c/luatexdir/tex/textoken.w $";

#include "ptexlib.h"

/*:1*//*2:*/
#line 27 "./textoken.w"

#define pausing int_par(pausing_code)
#define cat_code_table int_par(cat_code_table_code)
#define tracing_nesting int_par(tracing_nesting_code)
#define end_line_char int_par(end_line_char_code)
#define suppress_outer_error int_par(suppress_outer_error_code)

#define every_eof equiv(every_eof_loc)
#define box(A) equiv(box_base+(A))

#define detokenized_line() (line_catcode_table==NO_CAT_TABLE)

#define do_get_cat_code(a,b) do {                                         \
    if (line_catcode_table!=DEFAULT_CAT_TABLE)                          \
      a= get_cat_code(line_catcode_table,b);                       \
    else                                                                \
      a= get_cat_code(cat_code_table,b);                           \
  } while (0)


/*:2*//*4:*/
#line 66 "./textoken.w"

smemory_word*fixmem;
unsigned fix_mem_min;
unsigned fix_mem_max;


/*:4*//*5:*/
#line 78 "./textoken.w"

int var_used,dyn_used;

halfword avail;
unsigned fix_mem_end;

halfword garbage;
halfword temp_token_head;
halfword hold_token_head;
halfword omit_template;
halfword null_list;
halfword backup_head;

/*:5*//*6:*/
#line 91 "./textoken.w"

void initialize_tokens(void)
{
halfword p;
avail= null;
fix_mem_end= 0;
p= get_avail();
temp_token_head= p;
set_token_info(temp_token_head,0);
p= get_avail();
hold_token_head= p;
set_token_info(hold_token_head,0);
p= get_avail();
omit_template= p;
set_token_info(omit_template,0);
p= get_avail();
null_list= p;
set_token_info(null_list,0);
p= get_avail();
backup_head= p;
set_token_info(backup_head,0);
p= get_avail();
garbage= p;
set_token_info(garbage,0);
dyn_used= 0;
}

/*:6*//*7:*/
#line 127 "./textoken.w"

halfword get_avail(void)
{
unsigned p;
unsigned t;
p= (unsigned)avail;
if(p!=null){
avail= token_link(avail);
}else if(fix_mem_end<fix_mem_max){
incr(fix_mem_end);
p= fix_mem_end;
}else{
smemory_word*new_fixmem;
t= (fix_mem_max/5);
new_fixmem= 
fixmemcast(realloc
(fixmem,sizeof(smemory_word)*(fix_mem_max+t+1)));
if(new_fixmem==NULL){
runaway();
overflow("token memory size",fix_mem_max);
}else{
fixmem= new_fixmem;
}
memset(voidcast(fixmem+fix_mem_max+1),0,t*sizeof(smemory_word));
fix_mem_max+= t;
p= ++fix_mem_end;
}
token_link(p)= null;
incr(dyn_used);
return(halfword)p;
}


/*:7*//*8:*/
#line 164 "./textoken.w"

void flush_list(halfword p)
{
halfword q,r;
if(p!=null){
r= p;
do{
q= r;
r= token_link(r);
decr(dyn_used);
}while(r!=null);
token_link(q)= avail;
avail= p;
}
}

/*:8*//*10:*/
#line 247 "./textoken.w"

void print_meaning(void)
{
print_cmd_chr((quarterword)cur_cmd,cur_chr);
if(cur_cmd>=call_cmd){
print_char(':');
print_ln();
token_show(cur_chr);
}else{

if((cur_cmd==top_bot_mark_cmd)&&(cur_chr<marks_code)){
print_char(':');
print_ln();
switch(cur_chr){
case first_mark_code:
token_show(first_mark(0));
break;
case bot_mark_code:
token_show(bot_mark(0));
break;
case split_first_mark_code:
token_show(split_first_mark(0));
break;
case split_bot_mark_code:
token_show(split_bot_mark(0));
break;
default:
token_show(top_mark(0));
break;
}
}
}
}


/*:10*//*11:*/
#line 308 "./textoken.w"

void show_token_list(int p,int q,int l)
{
int m,c;
ASCII_code match_chr;
ASCII_code n;
match_chr= '#';
n= '0';
tally= 0;
if(l<0)
l= 0x3FFFFFFF;
while((p!=null)&&(tally<l)){
if(p==q){

set_trick_count();
}

if((p<(int)fix_mem_min)||(p> (int)fix_mem_end)){
tprint_esc("CLOBBERED.");
return;
}
if(token_info(p)>=cs_token_flag){
if(!((inhibit_par_tokens)&&(token_info(p)==par_token)))
print_cs(token_info(p)-cs_token_flag);
}else{
m= token_cmd(token_info(p));
c= token_chr(token_info(p));
if(token_info(p)<0){
tprint_esc("BAD.");
}else{




switch(m){
case left_brace_cmd:
case right_brace_cmd:
case math_shift_cmd:
case tab_mark_cmd:
case sup_mark_cmd:
case sub_mark_cmd:
case spacer_cmd:
case letter_cmd:
case other_char_cmd:
print(c);
break;
case mac_param_cmd:
if(!in_lua_escape)
print(c);
print(c);
break;
case out_param_cmd:
print(match_chr);
if(c<=9){
print_char(c+'0');
}else{
print_char('!');
return;
}
break;
case match_cmd:
match_chr= c;
print(c);
incr(n);
print_char(n);
if(n> '9')
return;
break;
case end_match_cmd:
if(c==0)
tprint("->");
break;
default:
tprint_esc("BAD.");
break;
}
}
}
p= token_link(p);
}
if(p!=null)
tprint_esc("ETC.");
}

/*:11*//*12:*/
#line 392 "./textoken.w"

#define do_buffer_to_unichar(a,b)  do {                         \
        a =  (halfword)str2uni(buffer+b);                        \
        b +=  utf8_size(a);                                      \
    } while (0)


/*:12*//*13:*/
#line 402 "./textoken.w"

void token_show(halfword p)
{
if(p!=null)
show_token_list(token_link(p),null,10000000);
}



/*:13*//*14:*/
#line 417 "./textoken.w"

void delete_token_ref(halfword p)
{

assert(token_ref_count(p)>=0);
if(token_ref_count(p)==0)
flush_list(p);
else
decr(token_ref_count(p));
}

/*:14*//*15:*/
#line 428 "./textoken.w"

int get_char_cat_code(int curchr)
{
int a;
do_get_cat_code(a,curchr);
return a;
}

/*:15*//*16:*/
#line 436 "./textoken.w"

static void invalid_character_error(void)
{
const char*hlp[]= 
{"A funny symbol that I can't read has just been input.",
"Continue, and I'll forget that it ever happened.",
NULL
};
deletions_allowed= false;
tex_error("Text line contains an invalid character",hlp);
deletions_allowed= true;
}

/*:16*//*17:*/
#line 449 "./textoken.w"

static boolean process_sup_mark(void);

static int scan_control_sequence(void);

typedef enum{next_line_ok,next_line_return,
next_line_restart
}next_line_retval;

static next_line_retval next_line(void);


/*:17*//*18:*/
#line 476 "./textoken.w"

boolean scan_keyword(const char*s)
{
halfword p;
halfword q;
const char*k;
halfword save_cur_cs= cur_cs;
int saved_align_state= align_state;
assert(strlen(s)> 1);
p= backup_head;
token_link(p)= null;
k= s;
while(*k){
get_x_token();
if((cur_cs==0)&&
((cur_chr==*k)||(cur_chr==*k-'a'+'A'))){
store_new_token(cur_tok);
k++;
}else if((cur_cmd!=spacer_cmd)||(p!=backup_head)){
if(p!=backup_head){
q= get_avail();
token_info(q)= cur_tok;
token_link(q)= null;
token_link(p)= q;
begin_token_list(token_link(backup_head),backed_up);
if(cur_cmd!=endv_cmd)
align_state= saved_align_state;
}else{
back_input();
}
cur_cs= save_cur_cs;
return false;
}
}
flush_list(token_link(backup_head));
cur_cs= save_cur_cs;
if(cur_cmd!=endv_cmd)
align_state= saved_align_state;
return true;
}

/*:18*//*19:*/
#line 520 "./textoken.w"

halfword active_to_cs(int curchr,int force)
{
halfword curcs;
char*a,*b;
char*utfbytes= xmalloc(10);
int nncs= no_new_control_sequence;
a= (char*)uni2str(0xFFFF);
utfbytes= strcpy(utfbytes,a);
if(force)
no_new_control_sequence= false;
if(curchr> 0){
b= (char*)uni2str((unsigned)curchr);
utfbytes= strcat(utfbytes,b);
free(b);
curcs= string_lookup(utfbytes,strlen(utfbytes));
}else{
utfbytes[3]= '\0';
curcs= string_lookup(utfbytes,4);
}
no_new_control_sequence= nncs;
free(a);
free(utfbytes);
return curcs;
}

/*:19*//*20:*/
#line 548 "./textoken.w"

static char*cs_to_string(halfword p)
{
const char*s;
char*sh;
int k= 0;
static char ret[256]= {0};
if(p==0||p==null_cs){
ret[k++]= '\\';
s= "csname";
while(*s){
ret[k++]= *s++;
}
ret[k++]= '\\';
s= "endcsname";
while(*s){
ret[k++]= *s++;
}
ret[k]= 0;

}else{
str_number txt= cs_text(p);
sh= makecstring(txt);
s= sh;
if(is_active_cs(txt)){
s= s+3;
while(*s){
ret[k++]= *s++;
}
ret[k]= 0;
}else{
ret[k++]= '\\';
while(*s){
ret[k++]= *s++;
}
ret[k]= 0;
}
free(sh);
}
return(char*)ret;
}

/*:20*//*21:*/
#line 592 "./textoken.w"

static char*cmd_chr_to_string(int cmd,int chr)
{
char*s;
str_number str;
int sel= selector;
selector= new_string;
print_cmd_chr((quarterword)cmd,chr);
str= make_string();
s= makecstring(str);
selector= sel;
flush_str(str);
return s;
}

/*:21*//*23:*/
#line 635 "./textoken.w"

halfword par_loc;
halfword par_token;


/*:23*//*25:*/
#line 649 "./textoken.w"

boolean force_eof;
int luacstrings;


/*:25*//*26:*/
#line 661 "./textoken.w"

void firm_up_the_line(void)
{
int k;
ilimit= last;
if(pausing> 0){
if(interaction> nonstop_mode){
wake_up_terminal();
print_ln();
if(istart<ilimit){
for(k= istart;k<=ilimit-1;k++)
print_char(buffer[k]);
}
first= ilimit;
prompt_input("=>");
if(last> first){
for(k= first;k<+last-1;k++)
buffer[k+istart-first]= buffer[k];
ilimit= istart+last-first;
}
}
}
}



/*:26*//*27:*/
#line 692 "./textoken.w"

void check_outer_validity(void)
{
halfword p;
halfword q;
if(suppress_outer_error)
return;
if(scanner_status!=normal){
deletions_allowed= false;



if(cur_cs!=0){
if((istate==token_list)||(iname<1)||(iname> 17)){
p= get_avail();
token_info(p)= cs_token_flag+cur_cs;
begin_token_list(p,backed_up);
}
cur_cmd= spacer_cmd;
cur_chr= ' ';
}
if(scanner_status> skipping){
const char*errhlp[]= 
{"I suspect you have forgotten a `}', causing me",
"to read past where you wanted me to stop.",
"I'll try to recover; but if the error is serious,",
"you'd better type `E' or `X' now and fix your file.",
NULL
};
char errmsg[256];
const char*startmsg;
const char*scannermsg;

runaway();
if(cur_cs==0){
startmsg= "File ended";
}else{
cur_cs= 0;
startmsg= "Forbidden control sequence found";
}








p= get_avail();
switch(scanner_status){
case defining:
scannermsg= "definition";
token_info(p)= right_brace_token+'}';
break;
case matching:
scannermsg= "use";
token_info(p)= par_token;
long_state= outer_call_cmd;
break;
case aligning:
scannermsg= "preamble";
token_info(p)= right_brace_token+'}';
q= p;
p= get_avail();
token_link(p)= q;
token_info(p)= cs_token_flag+frozen_cr;
align_state= -1000000;
break;
case absorbing:
scannermsg= "text";
token_info(p)= right_brace_token+'}';
break;
default:
scannermsg= "unknown";
break;
}
begin_token_list(p,inserted);
snprintf(errmsg,255,"%s while scanning %s of %s",
startmsg,scannermsg,cs_to_string(warning_index));
tex_error(errmsg,errhlp);
}else{
char errmsg[256];
const char*errhlp_no[]= 
{"The file ended while I was skipping conditional text.",
"This kind of error happens when you say `\\if...' and forget",
"the matching `\\fi'. I've inserted a `\\fi'; this might work.",
NULL
};
const char*errhlp_cs[]= 
{"A forbidden control sequence occurred in skipped text.",
"This kind of error happens when you say `\\if...' and forget",
"the matching `\\fi'. I've inserted a `\\fi'; this might work.",
NULL
};
const char**errhlp= (const char**)errhlp_no;
char*ss;
if(cur_cs!=0){
errhlp= errhlp_cs;
cur_cs= 0;
}
ss= cmd_chr_to_string(if_test_cmd,cur_if);
snprintf(errmsg,255,
"Incomplete %s; all text was ignored after line %d",
ss,(int)skip_line);
free(ss);

cur_tok= cs_token_flag+frozen_fi;

{
OK_to_interrupt= false;
back_input();
token_type= inserted;
OK_to_interrupt= true;
tex_error(errmsg,errhlp);
}
}
deletions_allowed= true;
}
}

/*:27*//*28:*/
#line 812 "./textoken.w"

static boolean get_next_file(void)
{
SWITCH:
if(iloc<=ilimit){
do_buffer_to_unichar(cur_chr,iloc);

RESWITCH:
if(detokenized_line()){
cur_cmd= (cur_chr==' '?10:12);
}else{
do_get_cat_code(cur_cmd,cur_chr);
}











switch(istate+cur_cmd){
case mid_line+ignore_cmd:
case skip_blanks+ignore_cmd:
case new_line+ignore_cmd:
case skip_blanks+spacer_cmd:
case new_line+spacer_cmd:
goto SWITCH;
break;
case mid_line+escape_cmd:
case new_line+escape_cmd:
case skip_blanks+escape_cmd:
istate= (unsigned char)scan_control_sequence();
if(cur_cmd>=outer_call_cmd)
check_outer_validity();
break;
case mid_line+active_char_cmd:
case new_line+active_char_cmd:
case skip_blanks+active_char_cmd:
cur_cs= active_to_cs(cur_chr,false);
cur_cmd= eq_type(cur_cs);
cur_chr= equiv(cur_cs);
istate= mid_line;
if(cur_cmd>=outer_call_cmd)
check_outer_validity();
break;
case mid_line+sup_mark_cmd:
case new_line+sup_mark_cmd:
case skip_blanks+sup_mark_cmd:
if(process_sup_mark())
goto RESWITCH;
else
istate= mid_line;
break;
case mid_line+invalid_char_cmd:
case new_line+invalid_char_cmd:
case skip_blanks+invalid_char_cmd:
invalid_character_error();
return false;
break;
case mid_line+spacer_cmd:
istate= skip_blanks;
cur_chr= ' ';
break;
case mid_line+car_ret_cmd:






iloc= ilimit+1;
cur_cmd= spacer_cmd;
cur_chr= ' ';
break;
case skip_blanks+car_ret_cmd:
case mid_line+comment_cmd:
case new_line+comment_cmd:
case skip_blanks+comment_cmd:
iloc= ilimit+1;
goto SWITCH;
break;
case new_line+car_ret_cmd:
iloc= ilimit+1;
cur_cs= par_loc;
cur_cmd= eq_type(cur_cs);
cur_chr= equiv(cur_cs);
if(cur_cmd>=outer_call_cmd)
check_outer_validity();
break;
case skip_blanks+left_brace_cmd:
case new_line+left_brace_cmd:
istate= mid_line;
case mid_line+left_brace_cmd:
align_state++;
break;
case skip_blanks+right_brace_cmd:
case new_line+right_brace_cmd:
istate= mid_line;
case mid_line+right_brace_cmd:
align_state--;
break;
case mid_line+math_shift_cmd:
case mid_line+tab_mark_cmd:
case mid_line+mac_param_cmd:
case mid_line+sub_mark_cmd:
case mid_line+letter_cmd:
case mid_line+other_char_cmd:
break;
#if 0
case skip_blanks+math_shift:
case skip_blanks+tab_mark:
case skip_blanks+mac_param:
case skip_blanks+sub_mark:
case skip_blanks+letter:
case skip_blanks+other_char:
case new_line+math_shift:
case new_line+tab_mark:
case new_line+mac_param:
case new_line+sub_mark:
case new_line+letter:
case new_line+other_char:
#else
default:
#endif
istate= mid_line;
break;
}
}else{
if(iname!=21)
istate= new_line;






do{
next_line_retval r= next_line();
if(r==next_line_return){
return true;
}else if(r==next_line_restart){
return false;
}
}while(0);
check_interrupt();
goto SWITCH;
}
return true;
}

/*:28*//*29:*/
#line 966 "./textoken.w"

#define is_hex(a) ((a>='0'&&a<='9')||(a>='a'&&a<='f'))

#define add_nybble(a)   do {                                            \
    if (a<='9') cur_chr= (cur_chr<<4)+a-'0';                             \
    else        cur_chr= (cur_chr<<4)+a-'a'+10;                          \
  } while (0)

#define hex_to_cur_chr do {                                             \
    if (c<='9')  cur_chr= c-'0';                                         \
    else         cur_chr= c-'a'+10;                                      \
    add_nybble(cc);                                                     \
  } while (0)

#define four_hex_to_cur_chr do {                                        \
    hex_to_cur_chr;                                                     \
    add_nybble(ccc); add_nybble(cccc);                                  \
  } while (0)

#define five_hex_to_cur_chr  do {                                       \
    four_hex_to_cur_chr;                                                \
    add_nybble(ccccc);                                                  \
  } while (0)

#define six_hex_to_cur_chr do {                                         \
    five_hex_to_cur_chr;                                                \
    add_nybble(cccccc);                                                 \
  } while (0)


/*:29*//*30:*/
#line 998 "./textoken.w"

static boolean process_sup_mark(void)
{
if(cur_chr==buffer[iloc]){
int c,cc;
if(iloc<ilimit){
if((cur_chr==buffer[iloc+1])&&(cur_chr==buffer[iloc+2])
&&(cur_chr==buffer[iloc+3])
&&(cur_chr==buffer[iloc+4])
&&((iloc+10)<=ilimit)){
int ccc,cccc,ccccc,cccccc;
c= buffer[iloc+5];
cc= buffer[iloc+6];
ccc= buffer[iloc+7];
cccc= buffer[iloc+8];
ccccc= buffer[iloc+9];
cccccc= buffer[iloc+10];
if((is_hex(c))&&(is_hex(cc))&&(is_hex(ccc))
&&(is_hex(cccc))
&&(is_hex(ccccc))&&(is_hex(cccccc))){
iloc= iloc+11;
six_hex_to_cur_chr;
return true;
}
}
if((cur_chr==buffer[iloc+1])&&(cur_chr==buffer[iloc+2])
&&(cur_chr==buffer[iloc+3])&&((iloc+8)<=ilimit)){
int ccc,cccc,ccccc;
c= buffer[iloc+4];
cc= buffer[iloc+5];
ccc= buffer[iloc+6];
cccc= buffer[iloc+7];
ccccc= buffer[iloc+8];
if((is_hex(c))&&(is_hex(cc))&&(is_hex(ccc))
&&(is_hex(cccc))&&(is_hex(ccccc))){
iloc= iloc+9;
five_hex_to_cur_chr;
return true;
}
}
if((cur_chr==buffer[iloc+1])&&(cur_chr==buffer[iloc+2])
&&((iloc+6)<=ilimit)){
int ccc,cccc;
c= buffer[iloc+3];
cc= buffer[iloc+4];
ccc= buffer[iloc+5];
cccc= buffer[iloc+6];
if((is_hex(c))&&(is_hex(cc))&&(is_hex(ccc))
&&(is_hex(cccc))){
iloc= iloc+7;
four_hex_to_cur_chr;
return true;
}
}
c= buffer[iloc+1];
if(c<0200){
iloc= iloc+2;
if(is_hex(c)&&iloc<=ilimit){
cc= buffer[iloc];
if(is_hex(cc)){
incr(iloc);
hex_to_cur_chr;
return true;
}
}
cur_chr= (c<0100?c+0100:c-0100);
return true;
}
}
}
return false;
}

/*:30*//*31:*/
#line 1086 "./textoken.w"

static boolean check_expanded_code(int*kk);

static int scan_control_sequence(void)
{
int retval= mid_line;
if(iloc> ilimit){
cur_cs= null_cs;
}else{
register int cat;
while(1){
int k= iloc;
do_buffer_to_unichar(cur_chr,k);
do_get_cat_code(cat,cur_chr);
if(cat!=letter_cmd||k> ilimit){
retval= (cat==spacer_cmd?skip_blanks:mid_line);
if(cat==sup_mark_cmd&&check_expanded_code(&k))
continue;
}else{
retval= skip_blanks;
do{
do_buffer_to_unichar(cur_chr,k);
do_get_cat_code(cat,cur_chr);
}while(cat==letter_cmd&&k<=ilimit);

if(cat==sup_mark_cmd&&check_expanded_code(&k))
continue;
if(cat!=letter_cmd){
decr(k);
if(cur_chr> 0xFFFF)
decr(k);
if(cur_chr> 0x7FF)
decr(k);
if(cur_chr> 0x7F)
decr(k);
}
}
cur_cs= id_lookup(iloc,k-iloc);
iloc= k;
break;
}
}
cur_cmd= eq_type(cur_cs);
cur_chr= equiv(cur_cs);
return retval;
}

/*:31*//*32:*/
#line 1140 "./textoken.w"

static boolean check_expanded_code(int*kk)
{
int l;
int k= *kk;
int d= 1;
int c,cc,ccc,cccc,ccccc,cccccc;
if(buffer[k]==cur_chr&&k<ilimit){
if((cur_chr==buffer[k+1])&&(cur_chr==buffer[k+2])
&&((k+6)<=ilimit)){
d= 4;
if((cur_chr==buffer[k+3])&&((k+8)<=ilimit))
d= 5;
if((cur_chr==buffer[k+4])&&((k+10)<=ilimit))
d= 6;
c= buffer[k+d-1];
cc= buffer[k+d];
ccc= buffer[k+d+1];
cccc= buffer[k+d+2];
if(d==6){
ccccc= buffer[k+d+3];
cccccc= buffer[k+d+4];
if(is_hex(c)&&is_hex(cc)&&is_hex(ccc)&&is_hex(cccc)
&&is_hex(ccccc)&&is_hex(cccccc))
six_hex_to_cur_chr;
}else if(d==5){
ccccc= buffer[k+d+3];
if(is_hex(c)&&is_hex(cc)&&is_hex(ccc)&&is_hex(cccc)
&&is_hex(ccccc))
five_hex_to_cur_chr;
}else{
if(is_hex(c)&&is_hex(cc)&&is_hex(ccc)&&is_hex(cccc))
four_hex_to_cur_chr;
}
}else{
c= buffer[k+1];
if(c<0200){
d= 1;
if(is_hex(c)&&(k+2)<=ilimit){
cc= buffer[k+2];
if(is_hex(c)&&is_hex(cc)){
d= 2;
hex_to_cur_chr;
}
}else if(c<0100){
cur_chr= c+0100;
}else{
cur_chr= c-0100;
}
}
}
if(d> 2)
d= 2*d-1;
else
d++;
if(cur_chr<=0x7F){
buffer[k-1]= (packed_ASCII_code)cur_chr;
}else if(cur_chr<=0x7FF){
buffer[k-1]= (packed_ASCII_code)(0xC0+cur_chr/0x40);
k++;
d--;
buffer[k-1]= (packed_ASCII_code)(0x80+cur_chr%0x40);
}else if(cur_chr<=0xFFFF){
buffer[k-1]= (packed_ASCII_code)(0xE0+cur_chr/0x1000);
k++;
d--;
buffer[k-1]= 
(packed_ASCII_code)(0x80+(cur_chr%0x1000)/0x40);
k++;
d--;
buffer[k-1]= 
(packed_ASCII_code)(0x80+(cur_chr%0x1000)%0x40);
}else{
buffer[k-1]= (packed_ASCII_code)(0xF0+cur_chr/0x40000);
k++;
d--;
buffer[k-1]= 
(packed_ASCII_code)(0x80+(cur_chr%0x40000)/0x1000);
k++;
d--;
buffer[k-1]= 
(packed_ASCII_code)(0x80+
((cur_chr%0x40000)%0x1000)/0x40);
k++;
d--;
buffer[k-1]= 
(packed_ASCII_code)(0x80+
((cur_chr%0x40000)%0x1000)%0x40);
}
l= k;
ilimit= ilimit-d;
while(l<=ilimit){
buffer[l]= buffer[l+d];
l++;
}
*kk= k;
return true;
}
return false;
}

/*:32*//*33:*/
#line 1244 "./textoken.w"

boolean end_line_char_inactive(void)
{
return((end_line_char<0)||(end_line_char> 127));
}

/*:33*//*34:*/
#line 1253 "./textoken.w"

static next_line_retval next_line(void)
{
boolean inhibit_eol= false;
if(iname> 17){

incr(line);
first= istart;
if(!force_eof){
if(iname<=20){
if(pseudo_input()){
firm_up_the_line();
line_catcode_table= DEFAULT_CAT_TABLE;
if((iname==19)&&(pseudo_lines(pseudo_files)==null))
inhibit_eol= true;
}else if((every_eof!=null)&&!eof_seen[iindex]){
ilimit= first-1;
eof_seen[iindex]= true;
if(iname!=19)
begin_token_list(every_eof,every_eof_text);
return next_line_restart;
}else{
force_eof= true;
}
}else{
if(iname==21){
if(luacstring_input()){
firm_up_the_line();
line_catcode_table= (short)luacstring_cattable();
line_partial= (signed char)luacstring_partial();
if(luacstring_final_line()||line_partial
||line_catcode_table==NO_CAT_TABLE)
inhibit_eol= true;
if(!line_partial)
istate= new_line;
}else{
force_eof= true;
}
}else{
if(lua_input_ln(cur_file,0,true)){
firm_up_the_line();
line_catcode_table= DEFAULT_CAT_TABLE;
}else if((every_eof!=null)&&(!eof_seen[iindex])){
ilimit= first-1;
eof_seen[iindex]= true;
begin_token_list(every_eof,every_eof_text);
return next_line_restart;
}else{
force_eof= true;
}
}
}
}
if(force_eof){
if(tracing_nesting> 0)
if((grp_stack[in_open]!=cur_boundary)
||(if_stack[in_open]!=cond_ptr))
if(!((iname==19)||(iname==21)))
file_warning();
if((iname> 21)||(iname==20)){
if(tracefilenames)
print_char(')');
open_parens--;
#if 0
update_terminal();
#endif
}
force_eof= false;
if(iname==21||
iname==19){
end_file_reading();
}else{
end_file_reading();
check_outer_validity();
}
return next_line_restart;
}
if(inhibit_eol||end_line_char_inactive())
ilimit--;
else
buffer[ilimit]= (packed_ASCII_code)end_line_char;
first= ilimit+1;
iloc= istart;
}else{
if(!terminal_input){
cur_cmd= 0;
cur_chr= 0;
return next_line_return;
}
if(input_ptr> 0){
end_file_reading();
return next_line_restart;
}
if(selector<log_only)
open_log_file();
if(interaction> nonstop_mode){
if(end_line_char_inactive())
ilimit++;
if(ilimit==istart){
tprint_nl("(Please type a command or say `\\end')");
}
print_ln();
first= istart;
prompt_input("*");
ilimit= last;
if(end_line_char_inactive())
ilimit--;
else
buffer[ilimit]= (packed_ASCII_code)end_line_char;
first= ilimit+1;
iloc= istart;
}else{
fatal_error("*** (job aborted, no legal \\end found)");


}
}
return next_line_ok;
}

/*:34*//*35:*/
#line 1375 "./textoken.w"

static boolean get_next_tokenlist(void)
{
register halfword t;
t= token_info(iloc);
iloc= token_link(iloc);
if(t>=cs_token_flag){
cur_cs= t-cs_token_flag;
cur_cmd= eq_type(cur_cs);
if(cur_cmd>=outer_call_cmd){
if(cur_cmd==dont_expand_cmd){




cur_cs= token_info(iloc)-cs_token_flag;
iloc= null;
cur_cmd= eq_type(cur_cs);
if(cur_cmd> max_command_cmd){
cur_cmd= relax_cmd;
cur_chr= no_expand_flag;
return true;
}
}else{
check_outer_validity();
}
}
cur_chr= equiv(cur_cs);
}else{
cur_cmd= token_cmd(t);
cur_chr= token_chr(t);
switch(cur_cmd){
case left_brace_cmd:
align_state++;
break;
case right_brace_cmd:
align_state--;
break;
case out_param_cmd:
begin_token_list(param_stack[param_start+cur_chr-1],parameter);
return false;
break;
}
}
return true;
}

/*:35*//*37:*/
#line 1428 "./textoken.w"

void get_next(void)
{
RESTART:
cur_cs= 0;
if(istate!=token_list){

if(!get_next_file())
goto RESTART;
}else{
if(iloc==null){
end_token_list();
goto RESTART;
}else if(!get_next_tokenlist()){
goto RESTART;
}
}

if((cur_cmd==tab_mark_cmd||cur_cmd==car_ret_cmd)&&align_state==0){
insert_vj_template();
goto RESTART;
}
}


/*:37*//*38:*/
#line 1476 "./textoken.w"

void get_token(void)
{
no_new_control_sequence= false;
get_token_lua();
no_new_control_sequence= true;
if(cur_cs==0)
cur_tok= token_val(cur_cmd,cur_chr);
else
cur_tok= cs_token_flag+cur_cs;
}

/*:38*//*39:*/
#line 1488 "./textoken.w"

void get_token_lua(void)
{
register int callback_id;
callback_id= callback_defined(token_filter_callback);
if(callback_id> 0){
while(istate==token_list&&iloc==null&&iindex!=v_template)
end_token_list();

if(!(istate==token_list&&
((nofilter==true)||(iindex==backed_up&&iloc!=null)))){
do_get_token_lua(callback_id);
return;
}
}
get_next();
}


/*:39*//*40:*/
#line 1508 "./textoken.w"

halfword string_to_toks(char*ss)
{
halfword p;
halfword q;
halfword t;
char*s= ss,*se= ss+strlen(s);
p= temp_token_head;
set_token_link(p,null);
while(s<se){
t= (halfword)str2uni((unsigned char*)s);
s+= utf8_size(t);
if(t==' ')
t= space_token;
else
t= other_token+t;
fast_store_new_token(t);
}
return token_link(temp_token_head);
}

/*:40*//*41:*/
#line 1543 "./textoken.w"

static halfword lua_str_toks(lstring b)
{
halfword p;
halfword q;
halfword t;
unsigned char*k;
p= temp_token_head;
set_token_link(p,null);
k= (unsigned char*)b.s;
while(k<(unsigned char*)b.s+b.l){
t= pool_to_unichar(k);
k+= utf8_size(t);
if(t==' '){
t= space_token;
}else{
if((t=='\\')||(t=='"')||(t=='\'')||(t==10)
||(t==13))
fast_store_new_token(other_token+'\\');
if(t==10)
t= 'n';
if(t==13)
t= 'r';
t= other_token+t;
}
fast_store_new_token(t);
}
return p;
}


/*:41*//*42:*/
#line 1577 "./textoken.w"

halfword str_toks(lstring s)
{
halfword p;
halfword q;
halfword t;
unsigned char*k,*l;
p= temp_token_head;
set_token_link(p,null);
k= s.s;
l= k+s.l;
while(k<l){
t= pool_to_unichar(k);
k+= utf8_size(t);
if(t==' ')
t= space_token;
else
t= other_token+t;
fast_store_new_token(t);
}
return p;
}

/*:42*//*43:*/
#line 1601 "./textoken.w"

void ins_the_toks(void)
{
(void)the_toks();
ins_list(token_link(temp_token_head));
}

/*:43*//*44:*/
#line 1611 "./textoken.w"

static void print_job_name(void)
{
if(job_name){
char*s,*ss;
int callback_id,lua_retval;
s= (char*)str_string(job_name);
callback_id= callback_defined(process_jobname_callback);
if(callback_id> 0){
lua_retval= run_callback(callback_id,"S->S",s,&ss);
if((lua_retval==true)&&(ss!=NULL))
s= ss;
}
tprint(s);
}else{
print(job_name);
}
}

/*:44*//*45:*/
#line 1635 "./textoken.w"

static boolean print_convert_string(halfword c,int i)
{
int ff;
boolean ret= true;
switch(c){
case number_code:
print_int(i);
break;
case uchar_code:
print(i);
break;
case roman_numeral_code:
print_roman_int(i);
break;
case etex_code:
tprint(eTeX_version_string);
break;
case pdftex_revision_code:
tprint(pdftex_revision);
break;
case luatex_revision_code:
print(get_luatexrevision());
break;
case luatex_date_code:
print_int(get_luatex_date_info());
break;
case pdftex_banner_code:
tprint(pdftex_banner);
break;
case uniform_deviate_code:
print_int(unif_rand(i));
break;
case normal_deviate_code:
print_int(norm_rand());
break;
case format_name_code:
print(format_name);
break;
case job_name_code:
print_job_name();
break;
case font_name_code:
append_string((unsigned char*)font_name(i),
(unsigned)strlen(font_name(i)));
if(font_size(i)!=font_dsize(i)){
tprint(" at ");
print_scaled(font_size(i));
tprint("pt");
}
break;
case font_id_code:
print_int(i);
break;
case math_style_code:
print_math_style();
break;
case pdf_font_name_code:
case pdf_font_objnum_code:
set_ff(i);
if(c==pdf_font_name_code)
print_int(obj_info(static_pdf,pdf_font_num(ff)));
else
print_int(pdf_font_num(ff));
break;
case pdf_font_size_code:
print_scaled(font_size(i));
tprint("pt");
break;
case pdf_page_ref_code:
print_int(pdf_get_obj(static_pdf,obj_type_page,i,false));
break;
case pdf_xform_name_code:
print_int(obj_info(static_pdf,i));
break;
case eTeX_revision_code:
tprint(eTeX_revision);
break;
default:
ret= false;
break;
}
return ret;
}

/*:45*//*46:*/
#line 1720 "./textoken.w"

int scan_lua_state(void)
{
int sn= 0;
if(scan_keyword("name")){
scan_pdf_ext_toks();
sn= def_ref;
}


do{
get_x_token();
}while((cur_cmd==spacer_cmd)||(cur_cmd==relax_cmd));

back_input();
if(cur_cmd!=left_brace_cmd){
scan_register_num();
if(get_lua_name(cur_val))
sn= (cur_val-65536);
}
return sn;
}


/*:46*//*47:*/
#line 1752 "./textoken.w"

void conv_toks(void)
{
int old_setting;
halfword p,q;
int save_scanner_status;
halfword save_def_ref;
halfword save_warning_index;
boolean bool;
str_number s;
int sn;
str_number u= 0;
int i= 0;
int j= 0;
int c= cur_chr;
str_number str;

switch(c){
case uchar_code:
scan_char_num();
break;
case number_code:
case roman_numeral_code:
scan_int();
break;
case string_code:
case meaning_code:
save_scanner_status= scanner_status;
scanner_status= normal;
get_token();
scanner_status= save_scanner_status;
break;
case etex_code:
break;
case font_name_code:
case font_id_code:
scan_font_ident();
break;
case pdftex_revision_code:
case luatex_revision_code:
case luatex_date_code:
case pdftex_banner_code:
break;
case pdf_font_name_code:
case pdf_font_objnum_code:
case pdf_font_size_code:
scan_font_ident();
if(cur_val==null_font)
pdf_error("font","invalid font identifier");
if(c!=pdf_font_size_code){
pdf_check_vf(cur_val);
if(!font_used(cur_val))
pdf_init_font(static_pdf,cur_val);
}
break;
case pdf_page_ref_code:
scan_int();
if(cur_val<=0)
pdf_error("pageref","invalid page number");
break;
case left_margin_kern_code:
case right_margin_kern_code:
scan_int();
if((box(cur_val)==null)||(type(box(cur_val))!=hlist_node))
pdf_error("marginkern","a non-empty hbox expected");
break;
case pdf_xform_name_code:
scan_int();
check_obj_type(static_pdf,obj_type_xform,cur_val);
break;
case pdf_creation_date_code:
ins_list(string_to_toks(getcreationdate(static_pdf)));
return;
break;
case format_name_code:
case job_name_code:
if(job_name==0)
open_log_file();
break;
case pdf_colorstack_init_code:
bool= scan_keyword("page");
if(scan_keyword("direct"))
cur_val= direct_always;
else if(scan_keyword("page"))
cur_val= direct_page;
else
cur_val= set_origin;
save_scanner_status= scanner_status;
save_warning_index= warning_index;
save_def_ref= def_ref;
u= save_cur_string();
scan_pdf_ext_toks();
s= tokens_to_string(def_ref);
delete_token_ref(def_ref);
def_ref= save_def_ref;
warning_index= save_warning_index;
scanner_status= save_scanner_status;
cur_val= newcolorstack(s,cur_val,bool);
flush_str(s);
cur_val_level= int_val_level;
if(cur_val<0){
print_err("Too many color stacks");
help2("The number of color stacks is limited to 32768.",
"I'll use the default color stack 0 here.");
error();
cur_val= 0;
restore_cur_string(u);
}
break;
case uniform_deviate_code:
scan_int();
break;
case normal_deviate_code:
break;
case lua_escape_string_code:
{
lstring escstr;
int l= 0;
save_scanner_status= scanner_status;
save_def_ref= def_ref;
save_warning_index= warning_index;
scan_pdf_ext_toks();
bool= in_lua_escape;
in_lua_escape= true;
escstr.s= (unsigned char*)tokenlist_to_cstring(def_ref,false,&l);
escstr.l= (unsigned)l;
in_lua_escape= bool;
delete_token_ref(def_ref);
def_ref= save_def_ref;
warning_index= save_warning_index;
scanner_status= save_scanner_status;
(void)lua_str_toks(escstr);
ins_list(token_link(temp_token_head));
free(escstr.s);
return;
}
break;
case math_style_code:
break;
case expanded_code:
save_scanner_status= scanner_status;
save_warning_index= warning_index;
save_def_ref= def_ref;
u= save_cur_string();
scan_pdf_ext_toks();
warning_index= save_warning_index;
scanner_status= save_scanner_status;
ins_list(token_link(def_ref));
def_ref= save_def_ref;
restore_cur_string(u);
return;
break;
case lua_code:
u= save_cur_string();
save_scanner_status= scanner_status;
save_def_ref= def_ref;
save_warning_index= warning_index;
sn= scan_lua_state();
scan_pdf_ext_toks();
s= def_ref;
warning_index= save_warning_index;
def_ref= save_def_ref;
scanner_status= save_scanner_status;
luacstrings= 0;
luatokencall(s,sn);
delete_token_ref(s);
restore_cur_string(u);
if(luacstrings> 0)
lua_string_start();
return;
break;
case pdf_insert_ht_code:
scan_register_num();
break;
case pdf_ximage_bbox_code:
scan_int();
check_obj_type(static_pdf,obj_type_ximage,cur_val);
i= obj_data_ptr(static_pdf,cur_val);
scan_int();
j= cur_val;
if((j<1)||(j> 4))
pdf_error("pdfximagebbox","invalid parameter");
break;

case eTeX_revision_code:
break;
default:
confusion("convert");
break;
}

old_setting= selector;
selector= new_string;


if(!print_convert_string(c,cur_val)){
switch(c){
case string_code:
if(cur_cs!=0)
sprint_cs(cur_cs);
else
print(cur_chr);
break;
case meaning_code:
print_meaning();
break;
case left_margin_kern_code:
p= list_ptr(box(cur_val));
if((p!=null)&&(!is_char_node(p))&&
(type(p)==glue_node)&&(subtype(p)==left_skip_code+1))
p= vlink(p);
if((p!=null)&&(!is_char_node(p))&&
(type(p)==margin_kern_node)&&(subtype(p)==left_side))
print_scaled(width(p));
else
print_char('0');
tprint("pt");
break;
case right_margin_kern_code:
q= list_ptr(box(cur_val));
p= null;
if(q!=null){
p= prev_rightmost(q,null);
if((p!=null)&&(!is_char_node(p))&&(type(p)==glue_node)
&&(subtype(p)==right_skip_code+1))
p= prev_rightmost(q,p);
}
if((p!=null)&&(!is_char_node(p))&&
(type(p)==margin_kern_node)&&(subtype(p)==right_side))
print_scaled(width(p));
else
print_char('0');
tprint("pt");
break;
case pdf_colorstack_init_code:
print_int(cur_val);
break;
case pdf_insert_ht_code:
i= cur_val;
p= page_ins_head;
while(i>=subtype(vlink(p)))
p= vlink(p);
if(subtype(p)==i)
print_scaled(height(p));
else
print_char('0');
tprint("pt");
break;
case pdf_ximage_bbox_code:
if(is_pdf_image(i)){
switch(j){
case 1:
print_scaled(epdf_orig_x(i));
break;
case 2:
print_scaled(epdf_orig_y(i));
break;
case 3:
print_scaled(epdf_orig_x(i)+epdf_xsize(i));
break;
case 4:
print_scaled(epdf_orig_y(i)+epdf_ysize(i));
break;
}
}else{
print_scaled(0);
}
tprint("pt");
break;
case pdf_creation_date_code:
case lua_escape_string_code:
case lua_code:
case expanded_code:
break;
default:
confusion("convert");
break;
}
}

selector= old_setting;
str= make_string();
(void)str_toks(str_lstring(str));
flush_str(str);
ins_list(token_link(temp_token_head));
}

/*:47*//*48:*/
#line 2040 "./textoken.w"

boolean in_lua_escape;

/*:48*//*49:*/
#line 2044 "./textoken.w"

boolean is_convert(halfword c)
{
return(c==convert_cmd);
}

str_number the_convert_string(halfword c,int i)
{
int old_setting;
str_number ret= 0;
old_setting= selector;
selector= new_string;
if(print_convert_string(c,i)){
ret= make_string();
}else if(c==font_identifier_code){
print_font_identifier(i);
ret= make_string();
}
selector= old_setting;
return ret;
}

/*:49*//*50:*/
#line 2073 "./textoken.w"

FILE*read_file[16];
int read_open[17];

void initialize_read(void)
{
int k;
for(k= 0;k<=16;k++)
read_open[k]= closed;
}

/*:50*//*51:*/
#line 2088 "./textoken.w"

void read_toks(int n,halfword r,halfword j)
{
halfword p;
halfword q;
int s;
int m;
scanner_status= defining;
warning_index= r;
p= get_avail();
def_ref= p;
set_token_ref_count(def_ref,0);
p= def_ref;
store_new_token(end_match_token);
if((n<0)||(n> 15))
m= 16;
else
m= n;
s= align_state;
align_state= 1000000;
do{

begin_file_reading();
iname= m+1;
if(read_open[m]==closed){




if(interaction> nonstop_mode){
if(n<0){
prompt_input("");
}else{
wake_up_terminal();
print_ln();
sprint_cs(r);
prompt_input(" =");
n= -1;
}
}else{
fatal_error
("*** (cannot \\read from terminal in nonstop modes)");
}

}else if(read_open[m]==just_open){



if(lua_input_ln(read_file[m],(m+1),false)){
read_open[m]= normal;
}else{
lua_a_close_in(read_file[m],(m+1));
read_open[m]= closed;
}

}else{


if(!lua_input_ln(read_file[m],(m+1),true)){
lua_a_close_in(read_file[m],(m+1));
read_open[m]= closed;
if(align_state!=1000000){
runaway();
print_err("File ended within \\read");
help1("This \\read has unbalanced braces.");
align_state= 1000000;
error();
}
}

}
ilimit= last;
if(end_line_char_inactive())
decr(ilimit);
else
buffer[ilimit]= (packed_ASCII_code)int_par(end_line_char_code);
first= ilimit+1;
iloc= istart;
istate= new_line;

if(j==1){
while(iloc<=ilimit){
cur_chr= buffer[iloc];
incr(iloc);
if(cur_chr==' ')
cur_tok= space_token;
else
cur_tok= cur_chr+other_token;
store_new_token(cur_tok);
}
}else{
while(1){
get_token();
if(cur_tok==0)
break;
if(align_state<1000000){
do{
get_token();
}while(cur_tok!=0);
align_state= 1000000;
break;
}
store_new_token(cur_tok);
}
}
end_file_reading();

}while(align_state!=1000000);
cur_val= def_ref;
scanner_status= normal;
align_state= s;
}

/*:51*//*52:*/
#line 2201 "./textoken.w"

str_number tokens_to_string(halfword p)
{
int old_setting;
if(selector==new_string)
pdf_error("tokens",
"tokens_to_string() called while selector = new_string");
old_setting= selector;
selector= new_string;
show_token_list(token_link(p),null,-1);
selector= old_setting;
return make_string();
}

/*:52*//*53:*/
#line 2215 "./textoken.w"

#define make_room(a)                                    \
    if ((unsigned)i+a+1> alloci) {                      \
        ret =  xrealloc(ret,(alloci+64));                \
        alloci =  alloci + 64;                           \
    }


#define append_i_byte(a) ret[i++] =  (char)(a)

#define Print_char(a) make_room(1); append_i_byte(a)

#define Print_uchar(s) {                                           \
    make_room(4);                                                  \
    if (s<=0x7F) {                                                 \
      append_i_byte(s);                                            \
    } else if (s<=0x7FF) {                                         \
      append_i_byte(0xC0 + (s / 0x40));                            \
      append_i_byte(0x80 + (s % 0x40));                            \
    } else if (s<=0xFFFF) {                                        \
      append_i_byte(0xE0 + (s / 0x1000));                          \
      append_i_byte(0x80 + ((s % 0x1000) / 0x40));                 \
      append_i_byte(0x80 + ((s % 0x1000) % 0x40));                 \
    } else if (s>=0x110000) {                                      \
      append_i_byte(s-0x11000);                                    \
    } else {                                                       \
      append_i_byte(0xF0 + (s / 0x40000));                         \
      append_i_byte(0x80 + ((s % 0x40000) / 0x1000));              \
      append_i_byte(0x80 + (((s % 0x40000) % 0x1000) / 0x40));     \
      append_i_byte(0x80 + (((s % 0x40000) % 0x1000) % 0x40));     \
    } }


#define Print_esc(b) {                                          \
    const char *v =  b;                                          \
    if (e> 0 && e<STRING_OFFSET) {                               \
        Print_uchar (e);                                        \
    }                                                           \
    make_room(strlen(v));                                       \
    while (*v) { append_i_byte(*v); v++; }                      \
  }

#define is_cat_letter(a)                                                \
    (get_char_cat_code(pool_to_unichar(str_string((a)))) == 11)

/*:53*//*54:*/
#line 2265 "./textoken.w"

char*tokenlist_to_cstring(int pp,int inhibit_par,int*siz)
{
register int p,c,m;
int q;
int infop;
char*s,*sh;
int e= 0;
char*ret;
int match_chr= '#';
int n= '0';
unsigned alloci= 1024;
int i= 0;
p= pp;
if(p==null){
if(siz!=NULL)
*siz= 0;
return NULL;
}
ret= xmalloc(alloci);
p= token_link(p);
if(p!=null){
e= int_par(escape_char_code);
}
while(p!=null){
if(p<(int)fix_mem_min||p> (int)fix_mem_end){
Print_esc("CLOBBERED.");
break;
}
infop= token_info(p);
if(infop>=cs_token_flag){
if(!(inhibit_par&&infop==par_token)){
q= infop-cs_token_flag;
if(q<hash_base){
if(q==null_cs){
Print_esc("csname");
Print_esc("endcsname");
}else{
Print_esc("IMPOSSIBLE.");
}
}else if((q>=undefined_control_sequence)
&&((q<=eqtb_size)
||(q> eqtb_size+hash_extra))){
Print_esc("IMPOSSIBLE.");
}else if((cs_text(q)<0)||(cs_text(q)>=str_ptr)){
Print_esc("NONEXISTENT.");
}else{
str_number txt= cs_text(q);
sh= makecstring(txt);
s= sh;
if(is_active_cs(txt)){
s= s+3;
while(*s){
Print_char(*s);
s++;
}
}else{
Print_uchar(e);
while(*s){
Print_char(*s);
s++;
}
if((!single_letter(txt))||is_cat_letter(txt)){
Print_char(' ');
}
}
free(sh);
}
}
}else{
if(infop<0){
Print_esc("BAD.");
}else{
m= token_cmd(infop);
c= token_chr(infop);
switch(m){
case left_brace_cmd:
case right_brace_cmd:
case math_shift_cmd:
case tab_mark_cmd:
case sup_mark_cmd:
case sub_mark_cmd:
case spacer_cmd:
case letter_cmd:
case other_char_cmd:
Print_uchar(c);
break;
case mac_param_cmd:
if(!in_lua_escape)
Print_uchar(c);
Print_uchar(c);
break;
case out_param_cmd:
Print_uchar(match_chr);
if(c<=9){
Print_char(c+'0');
}else{
Print_char('!');
goto EXIT;
}
break;
case match_cmd:
match_chr= c;
Print_uchar(c);
n++;
Print_char(n);
if(n> '9')
goto EXIT;
break;
case end_match_cmd:
if(c==0){
Print_char('-');
Print_char('>');
}
break;
default:
Print_esc("BAD.");
break;
}
}
}
p= token_link(p);
}
EXIT:
ret[i]= '\0';
if(siz!=NULL)
*siz= i;
return ret;
}

/*:54*//*55:*/
#line 2395 "./textoken.w"

lstring*tokenlist_to_lstring(int pp,int inhibit_par)
{
int siz;
lstring*ret= xmalloc(sizeof(lstring));
ret->s= (unsigned char*)tokenlist_to_cstring(pp,inhibit_par,&siz);
ret->l= (size_t)siz;
return ret;
}

/*:55*//*56:*/
#line 2405 "./textoken.w"

void free_lstring(lstring*ls)
{
if(ls==NULL)
return;
if(ls->s!=NULL)
free(ls->s);
free(ls);
}/*:56*/
