/*2:*/
#line 23 "./avlstuff.w"

static const char _svn_version[]= 
"$Id: avlstuff.w 4629 2013-04-05 10:54:08Z taco $"
"$URL: https://foundry.supelec.fr/svn/luatex/trunk/source/texk/web2c/luatexdir/utils/avlstuff.w $";

#include "ptexlib.h"
#include "utils/avl.h"

/*:2*//*3:*/
#line 32 "./avlstuff.w"

static void*avl_xmalloc(struct libavl_allocator*allocator,size_t size)
{
assert(allocator!=NULL&&size> 0);
return xmalloc((unsigned)size);
}

static void avl_xfree(struct libavl_allocator*allocator,void*block)
{
assert(allocator!=NULL&&block!=NULL);
xfree(block);
}

struct libavl_allocator avl_xallocator= {
avl_xmalloc,
avl_xfree
};

/*:3*//*4:*/
#line 51 "./avlstuff.w"

int comp_int_entry(const void*pa,const void*pb,void*p)
{
(void)p;
cmp_return(*(const int*)pa,*(const int*)pb);
return 0;
}

int comp_string_entry(const void*pa,const void*pb,void*p)
{
(void)p;
return strcmp((const char*)pa,(const char*)pb);
}/*:4*/
