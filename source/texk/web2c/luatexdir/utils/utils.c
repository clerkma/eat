/*1:*/
#line 21 "./utils.w"

static const char _svn_version[]= 
"$Id: utils.w 4629 2013-04-05 10:54:08Z taco $"
"$URL: https://foundry.supelec.fr/svn/luatex/trunk/source/texk/web2c/luatexdir/utils/utils.w $";

/*:1*//*2:*/
#line 26 "./utils.w"

#include <kpathsea/config.h>  

#include "sys/types.h"
#include <kpathsea/c-stat.h> 
#include <kpathsea/c-fopen.h> 
#include <string.h> 
#include <time.h> 
#include <float.h>               
#include "zlib.h"
#include "ptexlib.h"
#include "md5.h"

#include "lua/luatex-api.h"     

#include "png.h"



#include "poppler-config.h"

/*:2*//*3:*/
#line 47 "./utils.w"

#define check_nprintf(size_get, size_want) \
    if ((unsigned)(size_get) >= (unsigned)(size_want)) \
        pdftex_fail ("snprintf failed: file %s, line %d", __FILE__, __LINE__);

char*cur_file_name= NULL;
static char print_buf[PRINTF_BUF_SIZE];
int epochseconds;
int microseconds;


typedef char char_entry;
define_array(char);

/*:3*//*4:*/
#line 61 "./utils.w"

#define SUBSET_TAG_LENGTH 6
void make_subset_tag(fd_entry*fd)
{
int i,j= 0,a[SUBSET_TAG_LENGTH];
md5_state_t pms;
char*glyph;
glw_entry*glw_glyph;
struct avl_traverser t;
md5_byte_t digest[16];
void**aa;
static struct avl_table*st_tree= NULL;
if(st_tree==NULL)
st_tree= avl_create(comp_string_entry,NULL,&avl_xallocator);
assert(fd!=NULL);
assert(fd->gl_tree!=NULL);
assert(fd->fontname!=NULL);
assert(fd->subset_tag==NULL);
fd->subset_tag= xtalloc(SUBSET_TAG_LENGTH+1,char);
do{
md5_init(&pms);
avl_t_init(&t,fd->gl_tree);
if(is_cidkeyed(fd->fm)){
for(glw_glyph= (glw_entry*)avl_t_first(&t,fd->gl_tree);
glw_glyph!=NULL;glw_glyph= (glw_entry*)avl_t_next(&t)){
glyph= malloc(24);
sprintf(glyph,"%05u%05u ",glw_glyph->id,glw_glyph->wd);
md5_append(&pms,(md5_byte_t*)glyph,(int)strlen(glyph));
free(glyph);
}
}else{
for(glyph= (char*)avl_t_first(&t,fd->gl_tree);glyph!=NULL;
glyph= (char*)avl_t_next(&t)){
md5_append(&pms,(md5_byte_t*)glyph,(int)strlen(glyph));
md5_append(&pms,(const md5_byte_t*)" ",1);
}
}
md5_append(&pms,(md5_byte_t*)fd->fontname,
(int)strlen(fd->fontname));
md5_append(&pms,(md5_byte_t*)&j,sizeof(int));
md5_finish(&pms,digest);
for(a[0]= 0,i= 0;i<13;i++)
a[0]+= digest[i];
for(i= 1;i<SUBSET_TAG_LENGTH;i++)
a[i]= a[i-1]-digest[i-1]+digest[(i+12)%16];
for(i= 0;i<SUBSET_TAG_LENGTH;i++)
fd->subset_tag[i]= (char)(a[i]%26+'A');
fd->subset_tag[SUBSET_TAG_LENGTH]= '\0';
j++;
assert(j<100);
}
while((char*)avl_find(st_tree,fd->subset_tag)!=NULL);
aa= avl_probe(st_tree,fd->subset_tag);
assert(aa!=NULL);
if(j> 2)
pdftex_warn
("\nmake_subset_tag(): subset-tag collision, resolved in round %d.\n",
j);
}

/*:4*//*5:*/
#line 121 "./utils.w"

__attribute__((format(printf,1,2)))
void tex_printf(const char*fmt,...)
{
va_list args;
va_start(args,fmt);
vsnprintf(print_buf,PRINTF_BUF_SIZE,fmt,args);
tprint(print_buf);
xfflush(stdout);
va_end(args);
}

/*:5*//*6:*/
#line 141 "./utils.w"

__attribute__((noreturn,format(printf,1,2)))
void pdftex_fail(const char*fmt,...)
{
va_list args;
va_start(args,fmt);
print_ln();
tprint("!LuaTeX error");
if(cur_file_name){
tprint(" (file ");
tprint(cur_file_name);
tprint(")");
}
tprint(": ");
vsnprintf(print_buf,PRINTF_BUF_SIZE,fmt,args);
tprint(print_buf);
va_end(args);
print_ln();
remove_pdffile(static_pdf);
tprint(" ==> Fatal error occurred, no output PDF file produced!");
print_ln();
if(kpathsea_debug){
abort();
}else{
exit(EXIT_FAILURE);
}
}

/*:6*//*7:*/
#line 171 "./utils.w"

__attribute__((format(printf,1,2)))
void pdftex_warn(const char*fmt,...)
{
int old_selector= selector;
va_list args;
va_start(args,fmt);
selector= term_and_log;
print_ln();
tex_printf("LuaTeX warning");
if(cur_file_name)
tex_printf(" (file %s)",cur_file_name);
tex_printf(": ");
vsnprintf(print_buf,PRINTF_BUF_SIZE,fmt,args);
tprint(print_buf);
va_end(args);
print_ln();
selector= old_selector;
}

/*:7*//*8:*/
#line 191 "./utils.w"

void garbage_warning(void)
{
pdftex_warn("dangling objects discarded, no output file produced.");
remove_pdffile(static_pdf);
}

/*:8*//*9:*/
#line 198 "./utils.w"

char*pdftex_banner= NULL;

/*:9*//*10:*/
#line 201 "./utils.w"

void make_pdftex_banner(void)
{
char*s;
unsigned int slen;
int i;

if(pdftex_banner!=NULL)
return;

slen= (unsigned int)(SMALL_BUF_SIZE+
strlen(ptexbanner)+
strlen(versionstring)+
strlen(kpathsea_version_string));
s= xtalloc(slen,char);

i= snprintf(s,slen,
"%s%s %s",ptexbanner,versionstring,kpathsea_version_string);
check_nprintf(i,slen);
pdftex_banner= s;
}

/*:10*//*11:*/
#line 223 "./utils.w"

size_t xfwrite(void*ptr,size_t size,size_t nmemb,FILE*stream)
{
if(fwrite(ptr,size,nmemb,stream)!=nmemb)
pdftex_fail("fwrite() failed");
return nmemb;
}

/*:11*//*12:*/
#line 231 "./utils.w"

int xfflush(FILE*stream)
{
if(fflush(stream)!=0)
pdftex_fail("fflush() failed (%s)",strerror(errno));
return 0;
}

/*:12*//*13:*/
#line 239 "./utils.w"

int xgetc(FILE*stream)
{
int c= getc(stream);
if(c<0&&c!=EOF)
pdftex_fail("getc() failed (%s)",strerror(errno));
return c;
}

/*:13*//*14:*/
#line 248 "./utils.w"

int xputc(int c,FILE*stream)
{
int i= putc(c,stream);
if(i<0)
pdftex_fail("putc() failed (%s)",strerror(errno));
return i;
}

/*:14*//*15:*/
#line 257 "./utils.w"

scaled ext_xn_over_d(scaled x,scaled n,scaled d)
{
double r= (((double)x)*((double)n))/((double)d);
if(r> DBL_EPSILON)
r+= 0.5;
else
r-= 0.5;
if(r>=(double)max_integer||r<=-(double)max_integer)
pdftex_warn("arithmetic: number too big");
return(scaled)r;
}

/*:15*//*16:*/
#line 272 "./utils.w"

#if 0
char*stripzeros(char*a)
{
enum{NONUM,DOTNONUM,INT,DOT,LEADDOT,FRAC}s= NONUM,t= NONUM;
char*p,*q,*r;
for(p= q= r= a;*p!='\0';){
switch(s){
case NONUM:
if(*p>='0'&&*p<='9')
s= INT;
else if(*p=='.')
s= LEADDOT;
break;
case DOTNONUM:
if(*p!='.'&&(*p<'0'||*p> '9'))
s= NONUM;
break;
case INT:
if(*p=='.')
s= DOT;
else if(*p<'0'||*p> '9')
s= NONUM;
break;
case DOT:
case LEADDOT:
if(*p>='0'&&*p<='9')
s= FRAC;
else if(*p=='.')
s= DOTNONUM;
else
s= NONUM;
break;
case FRAC:
if(*p=='.')
s= DOTNONUM;
else if(*p<'0'||*p> '9')
s= NONUM;
break;
default:;
}
switch(s){
case DOT:
r= q;
break;
case LEADDOT:
r= q+1;
break;
case FRAC:
if(*p> '0')
r= q+1;
break;
case NONUM:
if((t==FRAC||t==DOT)&&r!=a){
q= r--;
if(*r=='.')
*r= '0';
r= a;
}
break;
default:;
}
*q++= *p++;
t= s;
}
*q= '\0';
return a;
}
#endif

/*:16*//*17:*/
#line 342 "./utils.w"

void initversionstring(char**versions)
{
const_string fmt= 
"Compiled with libpng %s; using libpng %s\n"
"Compiled with zlib %s; using zlib %s\n"
"Compiled with poppler version %s\n";
size_t len= strlen(fmt)
+strlen(PNG_LIBPNG_VER_STRING)+strlen(png_libpng_ver)
+strlen(ZLIB_VERSION)+strlen(zlib_version)
+strlen(POPPLER_VERSION)
+1;



*versions= xmalloc(len);
sprintf(*versions,fmt,
PNG_LIBPNG_VER_STRING,png_libpng_ver,
ZLIB_VERSION,zlib_version,POPPLER_VERSION);
}

/*:17*//*18:*/
#line 363 "./utils.w"

void check_buffer_overflow(int wsize)
{
if(wsize> buf_size){
int nsize= buf_size+buf_size/5+5;
if(nsize<wsize){
nsize= wsize+5;
}
buffer= 
(unsigned char*)xreallocarray(buffer,char,(unsigned)nsize);
buf_size= nsize;
}
}

/*:18*//*19:*/
#line 380 "./utils.w"

#define max_integer 0x7FFFFFFF

scaled divide_scaled(scaled s,scaled m,int dd)
{
register scaled q;
register scaled r;
int i;
int sign= 1;
if(s<0){
sign= -sign;
s= -s;
}
if(m<0){
sign= -sign;
m= -m;
}
if(m==0){
pdf_error("arithmetic","divided by zero");
}else if(m>=(max_integer/10)){
pdf_error("arithmetic","number too big");
}
q= s/m;
r= s%m;
for(i= 1;i<=(int)dd;i++){
q= 10*q+(10*r)/m;
r= (10*r)%m;
}

if(2*r>=m){
q++;
}
return sign*q;
}

/*:19*//*20:*/
#line 416 "./utils.w"

scaled divide_scaled_n(double sd,double md,double n)
{
double dd,di= 0.0;
dd= sd/md*n;
if(dd> 0.0)
di= floor(dd+0.5);
else if(dd<0.0)
di= -floor((-dd)+0.5);
return(scaled)di;
}

/*:20*//*21:*/
#line 428 "./utils.w"

int do_zround(double r)
{
int i;

if(r> 2147483647.0)
i= 2147483647;
else if(r<-2147483647.0)
i= -2147483647;
else if(r>=0.0)
i= (int)(r+0.5);
else
i= (int)(r-0.5);

return i;
}


/*:21*//*22:*/
#line 447 "./utils.w"

#ifdef MSVC

#  include <math.h> 
double rint(double x)
{
double c,f,d1,d2;

c= ceil(x);
f= floor(x);
d1= fabs(c-x);
d2= fabs(x-f);
if(d1> d2)
return f;
else
return c;
}

#endif/*:22*/
