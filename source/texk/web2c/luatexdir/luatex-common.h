#ifndef LUATEX_COMMON_H
#define LUATEX_COMMON_H
/* utils.c */
__attribute__ ((format(printf, 1, 2)))
extern void pdftex_warn(const char *fmt, ...);
__attribute__ ((noreturn, format(printf, 1, 2)))
extern void pdftex_fail(const char *fmt, ...);

#endif /* LUATEX_COMMON_H */
