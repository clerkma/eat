/*1:*/
#line 20 "./luagen.w"

static const char _svn_version[]= 
"$Id: luagen.w 4442 2012-05-25 22:40:34Z hhenkel $"
"$URL: https://foundry.supelec.fr/svn/luatex/trunk/source/texk/web2c/luatexdir/lua/luagen.w $";

#include "ptexlib.h"
#include "pdf/pdfpage.h"

/*:1*//*2:*/
#line 28 "./luagen.w"

void lua_begin_page(PDF pdf)
{
(void)pdf;
}

void lua_end_page(PDF pdf)
{
(void)pdf;
}

void lua_place_glyph(PDF pdf,internal_font_number f,int c)
{
(void)pdf;
(void)f;
printf("%c",(int)c);
}

void lua_place_rule(PDF pdf,scaledpos size)
{
(void)pdf;
(void)size;
}

void finish_lua_file(PDF pdf)
{
(void)pdf;
}/*:2*/
