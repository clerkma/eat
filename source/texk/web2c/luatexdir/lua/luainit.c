/*1:*/
#line 20 "./luainit.w"

static const char _svn_version[]= 
"$Id: luainit.w 4524 2012-12-20 15:38:02Z taco $"
"$URL: https://foundry.supelec.fr/svn/luatex/trunk/source/texk/web2c/luatexdir/lua/luainit.w $";

#include <kpathsea/c-stat.h> 

#include "ptexlib.h"
#include "lua/luatex-api.h"

/*:1*//*2:*/
#line 49 "./luainit.w"

const_string LUATEX_IHELP[]= {
"Usage: luatex --lua=FILE [OPTION]... [TEXNAME[.tex]] [COMMANDS]",
"   or: luatex --lua=FILE [OPTION]... \\FIRST-LINE",
"   or: luatex --lua=FILE [OPTION]... &FMT ARGS",
"  Run LuaTeX on TEXNAME, usually creating TEXNAME.pdf.",
"  Any remaining COMMANDS are processed as luatex input, after TEXNAME is read.",
"",
"  Alternatively, if the first non-option argument begins with a backslash,",
"  luatex interprets all non-option arguments as an input line.",
"",
"  Alternatively, if the first non-option argument begins with a &, the",
"  next word is taken as the FMT to read, overriding all else.  Any",
"  remaining arguments are processed as above.",
"",
"  If no arguments or options are specified, prompt for input.",
"",
"  The following regular options are understood: ",
"",
"   --8bit                        ignored, input is assumed to be in UTF-8 encoding",
"   --credits                     display credits and exit",
"   --debug-format                enable format debugging",
"   --default-translate-file=     ignored, input is assumed to be in UTF-8 encoding",
"   --disable-write18             disable \\write18{SHELL COMMAND}",
"   --draftmode                   switch on draft mode (generates no output PDF)",
"   --enable-write18              enable \\write18{SHELL COMMAND}",
"   --etex                        ignored, the etex extensions are always active",
"   --[no-]file-line-error        disable/enable file:line:error style messages",
"   --[no-]file-line-error-style  aliases of --[no-]file-line-error",
"   --fmt=FORMAT                  load the format file FORMAT",
"   --halt-on-error               stop processing at the first error",
"   --help                        display help and exit",
"   --ini                         be iniluatex, for dumping formats",
"   --interaction=STRING          set interaction mode (STRING=batchmode/nonstopmode/scrollmode/errorstopmode)",
"   --jobname=STRING              set the job name to STRING",
"   --kpathsea-debug=NUMBER       set path searching debugging flags according to the bits of NUMBER",
"   --lua=s                       load and execute a lua initialization script",
"   --[no-]mktex=FMT              disable/enable mktexFMT generation (FMT=tex/tfm)",
"   --nosocket                    disable the lua socket library",
"   --output-comment=STRING       use STRING for DVI file comment instead of date (no effect for PDF)",
"   --output-directory=DIR        use existing DIR as the directory to write files in",
"   --output-format=FORMAT        use FORMAT for job output; FORMAT is 'dvi' or 'pdf'",
"   --[no-]parse-first-line       disable/enable parsing of the first line of the input file",
"   --progname=STRING             set the program name to STRING",
"   --recorder                    enable filename recorder",
"   --safer                       disable easily exploitable lua commands",
"   --[no-]shell-escape           disable/enable \\write18{SHELL COMMAND}",
"   --shell-restricted            restrict \\write18 to a list of commands given in texmf.cnf",
"   --synctex=NUMBER              enable synctex",
"   --translate-file=             ignored, input is assumed to be in UTF-8 encoding",
"   --version                     display version and exit",
"",
"Alternate behaviour models can be obtained by special switches",
"",
"  --luaonly                run a lua file, then exit",
"  --luaconly               byte-compile a lua file, then exit",
"",
"See the reference manual for more information about the startup process.",
NULL
};

/*:2*//*3:*/
#line 111 "./luainit.w"

static char*ex_selfdir(char*argv0)
{
#if defined(WIN32)
#if defined(__MINGW32__)
char path[PATH_MAX],*fp;


if(SearchPath(NULL,argv0,".exe",PATH_MAX,path,NULL)==0)
FATAL1("Can't determine where the executable %s is.\n",argv0);

for(fp= path;fp&&*fp;fp++)
if(IS_DIR_SEP(*fp))
*fp= DIR_SEP;
#else 
#define PATH_MAX 512
char short_path[PATH_MAX],path[PATH_MAX],*fp;


if(SearchPath(NULL,argv0,".exe",PATH_MAX,short_path,&fp)==0)
FATAL1("Can't determine where the executable %s is.\n",argv0);
if(getlongpath(path,short_path,sizeof(path))==0){
FATAL1("This path points to an invalid file : %s\n",short_path);
}
#endif 
return xdirname(path);
#else 
return kpse_selfdir(argv0);
#endif
}

/*:3*//*4:*/
#line 142 "./luainit.w"

static void
prepare_cmdline(lua_State*L,char**av,int ac,int zero_offset)
{
int i;
char*s;
luaL_checkstack(L,ac+3,"too many arguments to script");
lua_createtable(L,0,0);
for(i= 0;i<ac;i++){
lua_pushstring(L,av[i]);
lua_rawseti(L,-2,(i-zero_offset));
}
lua_setglobal(L,"arg");
lua_getglobal(L,"os");
s= ex_selfdir(argv[0]);
lua_pushstring(L,s);
xfree(s);
lua_setfield(L,-2,"selfdir");
return;
}

/*:4*//*5:*/
#line 163 "./luainit.w"

string input_name= NULL;

static string user_progname= NULL;

char*startup_filename= NULL;
int lua_only= 0;
int lua_offset= 0;

int safer_option= 0;
int nosocket_option= 0;

/*:5*//*7:*/
#line 181 "./luainit.w"

#define ARGUMENT_IS(a) STREQ (long_options[option_index].name, a)


static struct option long_options[]
= {{"fmt",1,0,0},
{"lua",1,0,0},
{"luaonly",0,0,0},
{"safer",0,&safer_option,1},
{"nosocket",0,&nosocket_option,1},
{"help",0,0,0},
{"ini",0,&ini_version,1},
{"interaction",1,0,0},
{"halt-on-error",0,&haltonerrorp,1},
{"kpathsea-debug",1,0,0},
{"progname",1,0,0},
{"version",0,0,0},
{"credits",0,0,0},
{"recorder",0,&recorder_enabled,1},
{"etex",0,0,0},
{"output-comment",1,0,0},
{"output-directory",1,0,0},
{"draftmode",0,0,0},
{"output-format",1,0,0},
{"shell-escape",0,&shellenabledp,1},
{"no-shell-escape",0,&shellenabledp,-1},
{"enable-write18",0,&shellenabledp,1},
{"disable-write18",0,&shellenabledp,-1},
{"shell-restricted",0,0,0},
{"debug-format",0,&debug_format_file,1},
{"file-line-error-style",0,&filelineerrorstylep,1},
{"no-file-line-error-style",0,&filelineerrorstylep,-1},

{"file-line-error",0,&filelineerrorstylep,1},
{"no-file-line-error",0,&filelineerrorstylep,-1},
{"jobname",1,0,0},
{"parse-first-line",0,&parsefirstlinep,1},
{"no-parse-first-line",0,&parsefirstlinep,-1},
{"translate-file",1,0,0},
{"default-translate-file",1,0,0},
{"8bit",0,0,0},
{"mktex",1,0,0},
{"no-mktex",1,0,0},

{"synctex",1,0,0},
{0,0,0,0}
};

/*:7*//*8:*/
#line 229 "./luainit.w"

static void parse_options(int ac,char**av)
{
#ifdef WIN32

int sargc= argc;
char**sargv= argv;
#endif
int g;
int option_index;
char*firstfile= NULL;
opterr= 0;
if((strstr(argv[0],"luatexlua")!=NULL)||
(strstr(argv[0],"texlua")!=NULL)){
lua_only= 1;
luainit= 1;
}
for(;;){
g= getopt_long_only(ac,av,"+",long_options,&option_index);

if(g==-1)
break;
if(g=='?')
continue;

assert(g==0);

if(ARGUMENT_IS("luaonly")){
lua_only= 1;
lua_offset= optind;
luainit= 1;
}else if(ARGUMENT_IS("lua")){
startup_filename= optarg;
lua_offset= (optind-1);
luainit= 1;

}else if(ARGUMENT_IS("kpathsea-debug")){
kpathsea_debug|= atoi(optarg);

}else if(ARGUMENT_IS("progname")){
user_progname= optarg;

}else if(ARGUMENT_IS("jobname")){
c_job_name= optarg;

}else if(ARGUMENT_IS("fmt")){
dump_name= optarg;

}else if(ARGUMENT_IS("output-directory")){
output_directory= optarg;

}else if(ARGUMENT_IS("output-comment")){
size_t len= strlen(optarg);
if(len<256){
output_comment= optarg;
}else{
WARNING2("Comment truncated to 255 characters from %d. (%s)",
(int)len,optarg);
output_comment= (string)xmalloc(256);
strncpy(output_comment,optarg,255);
output_comment[255]= 0;
}

}else if(ARGUMENT_IS("shell-restricted")){
shellenabledp= 1;
restrictedshell= 1;

}else if(ARGUMENT_IS("output-format")){
pdf_output_option= 1;
if(strcmp(optarg,"dvi")==0){
pdf_output_value= 0;
}else if(strcmp(optarg,"pdf")==0){
pdf_output_value= 2;
}else{
WARNING1("Ignoring unknown value `%s' for --output-format",
optarg);
pdf_output_option= 0;
}

}else if(ARGUMENT_IS("draftmode")){
pdf_draftmode_option= 1;
pdf_draftmode_value= 1;

}else if(ARGUMENT_IS("mktex")){
kpse_maketex_option(optarg,true);

}else if(ARGUMENT_IS("no-mktex")){
kpse_maketex_option(optarg,false);

}else if(ARGUMENT_IS("interaction")){

if(STREQ(optarg,"batchmode")){
interactionoption= 0;
}else if(STREQ(optarg,"nonstopmode")){
interactionoption= 1;
}else if(STREQ(optarg,"scrollmode")){
interactionoption= 2;
}else if(STREQ(optarg,"errorstopmode")){
interactionoption= 3;
}else{
WARNING1("Ignoring unknown argument `%s' to --interaction",
optarg);
}

}else if(ARGUMENT_IS("synctex")){

synctexoption= (int)strtol(optarg,NULL,0);

}else if(ARGUMENT_IS("help")){
usagehelp(LUATEX_IHELP,BUG_ADDRESS);

}else if(ARGUMENT_IS("version")){
print_version_banner();

puts("\n\nExecute  'luatex --credits'  for credits and version details.\n\n"
"There is NO warranty. Redistribution of this software is covered by\n"
"the terms of the GNU General Public License, version 2. For more\n"
"information about these matters, see the file named COPYING and\n"
"the LuaTeX source.\n\n"
"Copyright 2011 Taco Hoekwater, the LuaTeX Team.\n");

uexit(0);
}else if(ARGUMENT_IS("credits")){
char*versions;
initversionstring(&versions);
print_version_banner();

puts("\n\nThe LuaTeX team is Hans Hagen, Hartmut Henkel, Taco Hoekwater.\n"
"LuaTeX merges and builds upon (parts of) the code from these projects:\n\n"
"tex       by Donald Knuth\n"
"etex      by Peter Breitenlohner, Phil Taylor and friends\n"
"omega     by John Plaice and Yannis Haralambous\n"
"aleph     by Giuseppe Bilotta\n"
"pdftex    by Han The Thanh and friends\n"
"kpathsea  by Karl Berry, Olaf Weber and others\n"
"lua       by Roberto Ierusalimschy, Waldemar Celes,\n"
"             Luiz Henrique de Figueiredo\n"
"metapost  by John Hobby, Taco Hoekwater and friends.\n"
"poppler   by Derek Noonburg, Kristian H\\ogsberg (partial)\n"
"fontforge by George Williams (partial)\n\n"
"Some extensions to lua and additional lua libraries are used, as well as\n"
"libraries for graphic inclusion. More details can be found in the source.\n"
"Code development was sponsored by a grant from Colorado State University\n"
"via the 'oriental tex' project, the TeX User Groups, and donations.\n");

puts(versions);
uexit(0);
}
}

if(lua_only){
if(argv[optind]){
startup_filename= xstrdup(argv[optind]);
lua_offset= optind;
}
}else if(argv[optind]&&argv[optind][0]=='&'){
dump_name= xstrdup(argv[optind]+1);
}else if(argv[optind]&&argv[optind][0]!='\\'){
if(argv[optind][0]=='*'){
input_name= xstrdup(argv[optind]+1);
}else{
firstfile= xstrdup(argv[optind]);
if((strstr(firstfile,".lua")==
firstfile+strlen(firstfile)-4)
||(strstr(firstfile,".luc")==
firstfile+strlen(firstfile)-4)
||(strstr(firstfile,".LUA")==
firstfile+strlen(firstfile)-4)
||(strstr(firstfile,".LUC")==
firstfile+strlen(firstfile)-4)){
if(startup_filename==NULL){
startup_filename= firstfile;
lua_offset= optind;
lua_only= 1;
luainit= 1;
}
}else{
input_name= firstfile;
}
}
#ifdef WIN32
}else if(sargv[sargc-1]&&sargv[sargc-1][0]!='-'&&
sargv[sargc-1][0]!='\\'){
if(sargv[sargc-1][0]=='&')
dump_name= xstrdup(sargv[sargc-1]+1);
else{
char*p;
if(sargv[sargc-1][0]=='*')
input_name= xstrdup(sargv[sargc-1]+1);
else
input_name= xstrdup(sargv[sargc-1]);
sargv[sargc-1]= normalize_quotes(input_name,"argument");



input_name+= xbasename(input_name)-input_name;
p= strrchr(input_name,'.');
if(p!=NULL&&strcasecmp(p,".tex")==0)
*p= '\0';
if(!c_job_name)
c_job_name= normalize_quotes(input_name,"jobname");
}
if(safer_option)
nosocket_option= 1;
return;
#endif
}
if(safer_option)
nosocket_option= 1;


if(input_name!=NULL){
argv[optind]= normalize_quotes(input_name,"argument");
}
}

/*:8*//*9:*/
#line 446 "./luainit.w"

#define is_readable(a) (stat(a,&finfo)==0) && S_ISREG(finfo.st_mode) &&  \
  (f= fopen(a,"r")) != NULL && !fclose(f)

/*:9*//*10:*/
#line 450 "./luainit.w"

static char*find_filename(char*name,const char*envkey)
{
struct stat finfo;
char*dirname= NULL;
char*filename= NULL;
FILE*f;
if(is_readable(name)){
return name;
}else{
dirname= getenv(envkey);
if((dirname!=NULL)&&strlen(dirname)){
dirname= xstrdup(getenv(envkey));
if(*(dirname+strlen(dirname)-1)=='/'){
*(dirname+strlen(dirname)-1)= 0;
}
filename= xmalloc((unsigned)(strlen(dirname)+strlen(name)+2));
filename= concat3(dirname,"/",name);
if(is_readable(filename)){
xfree(dirname);
return filename;
}
xfree(filename);
}
}
return NULL;
}


/*:10*//*11:*/
#line 479 "./luainit.w"

static char*cleaned_invocation_name(char*arg)
{
char*ret,*dot;
const char*start= xbasename(arg);
ret= xstrdup(start);
dot= index(ret,'.');
if(dot!=NULL){
*dot= 0;
}
return ret;
}

/*:11*//*12:*/
#line 492 "./luainit.w"

static void init_kpse(void)
{

if(!user_progname){
user_progname= dump_name;
}else if(!dump_name){
dump_name= user_progname;
}
if(!user_progname){
if(ini_version){
if(input_name){
char*p= input_name+strlen(input_name)-1;
while(p>=input_name){
if(IS_DIR_SEP(*p)){
p++;
input_name= p;
break;
}
p--;
}
user_progname= remove_suffix(input_name);
}
if(!user_progname){
user_progname= cleaned_invocation_name(argv[0]);
}
}else{
if(!dump_name){
dump_name= cleaned_invocation_name(argv[0]);
}
user_progname= dump_name;
}
}
kpse_set_program_enabled(kpse_fmt_format,MAKE_TEX_FMT_BY_DEFAULT,
kpse_src_compile);

kpse_set_program_name(argv[0],user_progname);
init_shell_escape();
program_name_set= 1;
}

/*:12*//*13:*/
#line 533 "./luainit.w"

static void fix_dumpname(void)
{
int dist;
if(dump_name){

dist= (int)(strlen(dump_name)-strlen(DUMP_EXT));
if(strstr(dump_name,DUMP_EXT)==dump_name+dist)
TEX_format_default= dump_name;
else
TEX_format_default= concat(dump_name,DUMP_EXT);
}else{

if(!ini_version)
abort();
}
}

/*:13*//*15:*/
#line 555 "./luainit.w"

static const char*luatex_kpse_find_aux(lua_State*L,const char*name,
kpse_file_format_type format,const char*errname)
{
const char*filename;
const char*altname;
altname= luaL_gsub(L,name,".","/");
filename= kpse_find_file(altname,format,false);
if(filename==NULL){
filename= kpse_find_file(name,format,false);
}
if(filename==NULL){
lua_pushfstring(L,"\n\t[kpse %s searcher] file not found: "LUA_QS,
errname,name);
}
return filename;
}

/*:15*//*16:*/
#line 582 "./luainit.w"

static int lua_loader_function= 0;

static int luatex_kpse_lua_find(lua_State*L)
{
const char*filename;
const char*name;
name= luaL_checkstring(L,1);
if(program_name_set==0){
lua_rawgeti(L,LUA_REGISTRYINDEX,lua_loader_function);
lua_pushvalue(L,-2);
lua_call(L,1,1);
return 1;
}
filename= luatex_kpse_find_aux(L,name,kpse_lua_format,"lua");
if(filename==NULL)
return 1;
if(luaL_loadfile(L,filename)!=0){
luaL_error(L,"error loading module %s from file %s:\n\t%s",
lua_tostring(L,1),filename,lua_tostring(L,-1));
}
return 1;
}

/*:16*//*17:*/
#line 606 "./luainit.w"

static int clua_loader_function= 0;
extern int searcher_C_luatex(lua_State*L,const char*name,const char*filename);

static int luatex_kpse_clua_find(lua_State*L)
{
const char*filename;
const char*name;
if(safer_option){
lua_pushliteral(L,"\n\t[C searcher disabled in safer mode]");
return 1;
}
name= luaL_checkstring(L,1);
if(program_name_set==0){
lua_rawgeti(L,LUA_REGISTRYINDEX,clua_loader_function);
lua_pushvalue(L,-2);
lua_call(L,1,1);
return 1;
}else{
const char*path_saved;
char*prefix,*postfix,*p,*total;
char*extensionless;
filename= luatex_kpse_find_aux(L,name,kpse_clua_format,"C");
if(filename==NULL)
return 1;
extensionless= strdup(filename);
if(!extensionless)return 1;
p= strstr(extensionless,name);
if(!p)return 1;
*p= '\0';
prefix= strdup(extensionless);
if(!prefix)return 1;
postfix= strdup(p+strlen(name));
if(!postfix)return 1;
total= malloc(strlen(prefix)+strlen(postfix)+2);
if(!total)return 1;
snprintf(total,strlen(prefix)+strlen(postfix)+2,"%s?%s",prefix,postfix);

lua_getglobal(L,"package");
lua_getfield(L,-1,"cpath");
path_saved= lua_tostring(L,-1);
lua_pop(L,1);

lua_pushstring(L,total);
lua_setfield(L,-2,"cpath");
lua_pop(L,1);

lua_rawgeti(L,LUA_REGISTRYINDEX,clua_loader_function);
lua_pushstring(L,name);
lua_call(L,1,1);

lua_getglobal(L,"package");
lua_pushstring(L,path_saved);
lua_setfield(L,-2,"cpath");
lua_pop(L,1);
free(extensionless);
free(total);
return 1;
}
}

/*:17*//*18:*/
#line 672 "./luainit.w"

static void setup_lua_path(lua_State*L)
{
lua_getglobal(L,"package");
lua_getfield(L,-1,"searchers");
lua_rawgeti(L,-1,2);
lua_loader_function= luaL_ref(L,LUA_REGISTRYINDEX);
lua_pushcfunction(L,luatex_kpse_lua_find);
lua_rawseti(L,-2,2);

lua_rawgeti(L,-1,3);
clua_loader_function= luaL_ref(L,LUA_REGISTRYINDEX);
lua_pushcfunction(L,luatex_kpse_clua_find);
lua_rawseti(L,-2,3);

lua_pop(L,2);
}

/*:18*//*19:*/
#line 692 "./luainit.w"

int tex_table_id;
int pdf_table_id;
int token_table_id;
int node_table_id;


#if defined(WIN32) || defined(__MINGW32__) || defined(__CYGWIN__)
char**suffixlist;

#  define EXE_SUFFIXES ".com;.exe;.bat;.cmd;.vbs;.vbe;.js;.jse;.wsf;.wsh;.ws;.tcl;.py;.pyw"

/*:19*//*20:*/
#line 704 "./luainit.w"

static void mk_suffixlist(void)
{
char**p;
char*q,*r,*v;
int n;

#  if defined(__CYGWIN__)
v= xstrdup(EXE_SUFFIXES);
#  else
v= (char*)getenv("PATHEXT");
if(v)
v= (char*)strlwr(xstrdup(v));
else
v= xstrdup(EXE_SUFFIXES);
#  endif

q= v;
n= 0;

while((r= strchr(q,';'))!=NULL){
n++;
r++;
q= r;
}
if(*q)
n++;
suffixlist= (char**)xmalloc((n+2)*sizeof(char*));
p= suffixlist;
*p= xstrdup(".dll");
p++;
q= v;
while((r= strchr(q,';'))!=NULL){
*r= '\0';
*p= xstrdup(q);
p++;
r++;
q= r;
}
if(*q){
*p= xstrdup(q);
p++;
}
*p= NULL;
free(v);
}
#endif

/*:20*//*21:*/
#line 752 "./luainit.w"

void lua_initialize(int ac,char**av)
{

char*given_file= NULL;
char*banner;
int kpse_init;
static char LC_CTYPE_C[]= "LC_CTYPE=C";
static char LC_COLLATE_C[]= "LC_COLLATE=C";
static char LC_NUMERIC_C[]= "LC_NUMERIC=C";
static char engine_luatex[]= "engine=luatex";

argc= ac;
argv= av;

if(luatex_svn<0){
const char*fmt= "This is LuaTeX, Version %s-%s"WEB2CVERSION;
size_t len;
char buf[16];
sprintf(buf,"%d",luatex_date_info);
len= strlen(fmt)+strlen(luatex_version_string)+strlen(buf)-3;



banner= xmalloc(len);
sprintf(banner,fmt,luatex_version_string,buf);
}else{
const char*fmt= "This is LuaTeX, Version %s-%s "WEB2CVERSION"(rev %d)";
size_t len;
char buf[16];
sprintf(buf,"%d",luatex_date_info);
len= strlen(fmt)+strlen(luatex_version_string)+strlen(buf)+6;
banner= xmalloc(len);
sprintf(banner,fmt,luatex_version_string,buf,luatex_svn);
}
ptexbanner= banner;

kpse_invocation_name= cleaned_invocation_name(argv[0]);


if(argc> 1&&
(STREQ(kpse_invocation_name,"texluac")||
STREQ(argv[1],"--luaconly")||STREQ(argv[1],"--luac"))){
exit(luac_main(ac,av));
}
#if defined(WIN32) || defined(__MINGW32__) || defined(__CYGWIN__)
mk_suffixlist();
#endif


interactionoption= 4;
dump_name= NULL;






#define SYNCTEX_NO_OPTION INT_MAX
synctexoption= SYNCTEX_NO_OPTION;


parse_options(ac,av);
if(lua_only)
shellenabledp= true;



putenv(LC_CTYPE_C);
putenv(LC_COLLATE_C);
putenv(LC_NUMERIC_C);


putenv(engine_luatex);

luainterpreter();

prepare_cmdline(Luas,argv,argc,lua_offset);
setup_lua_path(Luas);

if(startup_filename!=NULL){
given_file= xstrdup(startup_filename);
startup_filename= find_filename(startup_filename,"LUATEXDIR");
}

if(startup_filename!=NULL){
char*v1;

tex_table_id= hide_lua_table(Luas,"tex");
token_table_id= hide_lua_table(Luas,"token");
node_table_id= hide_lua_table(Luas,"node");
pdf_table_id= hide_lua_table(Luas,"pdf");

if(luaL_loadfile(Luas,startup_filename)){
fprintf(stdout,"%s\n",lua_tostring(Luas,-1));
exit(1);
}

init_tex_table(Luas);
if(lua_pcall(Luas,0,0,0)){
fprintf(stdout,"%s\n",lua_tostring(Luas,-1));
lua_traceback(Luas);
exit(1);
}

if(!input_name){
get_lua_string("texconfig","jobname",&input_name);
}
if(!dump_name){
get_lua_string("texconfig","formatname",&dump_name);
}
if(lua_only){
if(given_file)
free(given_file);

lua_close(Luas);
exit(0);
}

unhide_lua_table(Luas,"tex",tex_table_id);
unhide_lua_table(Luas,"pdf",pdf_table_id);
unhide_lua_table(Luas,"token",token_table_id);
unhide_lua_table(Luas,"node",node_table_id);


kpse_init= -1;
get_lua_boolean("texconfig","kpse_init",&kpse_init);

if(kpse_init!=0){
luainit= 0;
init_kpse();
}

tracefilenames= 1;
get_lua_boolean("texconfig","trace_file_names",&tracefilenames);


filelineerrorstylep= false;
get_lua_boolean("texconfig","file_line_error",&filelineerrorstylep);


haltonerrorp= false;
get_lua_boolean("texconfig","halt_on_error",&haltonerrorp);


v1= NULL;
get_lua_string("texconfig","shell_escape",&v1);
if(v1){
if(*v1=='t'||*v1=='y'||*v1=='1'){
shellenabledp= 1;
}else if(*v1=='p'){
shellenabledp= 1;
restrictedshell= 1;
}
free(v1);
}

if(shellenabledp&&restrictedshell==1){
v1= NULL;
get_lua_string("texconfig","shell_escape_commands",&v1);
if(v1){
mk_shellcmdlist(v1);
free(v1);
}
}

fix_dumpname();

}else{
if(luainit){
if(given_file){
fprintf(stdout,"%s file %s not found\n",
(lua_only?"Script":"Configuration"),given_file);
free(given_file);
}else{
fprintf(stdout,"No %s file given\n",
(lua_only?"script":"configuration"));
}
exit(1);
}else{

init_kpse();
fix_dumpname();
}
}
}

/*:21*//*22:*/
#line 939 "./luainit.w"

void check_texconfig_init(void)
{
if(Luas!=NULL){
lua_getglobal(Luas,"texconfig");
if(lua_istable(Luas,-1)){
lua_getfield(Luas,-1,"init");
if(lua_isfunction(Luas,-1)){
int i= lua_pcall(Luas,0,0,0);
if(i!=0){

fprintf(stderr,"This went wrong: %s\n",
lua_tostring(Luas,-1));
error();
}
}
}
}
}

/*:22*//*23:*/
#line 959 "./luainit.w"

void write_svnversion(char*v)
{
char*a_head,*n;
char*a= xstrdup(v);
size_t l= strlen("$Id: luatex.web ");
if(a!=NULL){
a_head= a;
if(strlen(a)> l)
a+= l;
n= a;
while(*n!='\0'&&*n!=' ')
n++;
*n= '\0';
fprintf(stdout," luatex.web >= v%s",a);
free(a_head);
}
}/*:23*/
