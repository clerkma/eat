/*1:*/
#line 20 "./luastuff.w"

static const char _svn_version[]= 
"$Id: luastuff.w 4605 2013-03-21 14:35:55Z taco $"
"$URL: https://foundry.supelec.fr/svn/luatex/trunk/source/texk/web2c/luatexdir/lua/luastuff.w $";

#include "ptexlib.h"
#include "lua/luatex-api.h"

/*:1*//*2:*/
#line 28 "./luastuff.w"

lua_State*Luas= NULL;

int luastate_bytes= 0;

int lua_active= 0;

/*:2*//*3:*/
#line 35 "./luastuff.w"

void make_table(lua_State*L,const char*tab,const char*getfunc,
const char*setfunc)
{

lua_pushstring(L,tab);
lua_newtable(L);
lua_settable(L,-3);

lua_pushstring(L,tab);
lua_gettable(L,-2);

luaL_newmetatable(L,tab);
lua_pushstring(L,"__index");
lua_pushstring(L,getfunc);
lua_gettable(L,-5);
lua_settable(L,-3);
lua_pushstring(L,"__newindex");
lua_pushstring(L,setfunc);
lua_gettable(L,-5);
lua_settable(L,-3);
lua_setmetatable(L,-2);
lua_pop(L,1);
}

/*:3*//*4:*/
#line 60 "./luastuff.w"

static
const char*getS(lua_State*L,void*ud,size_t*size)
{
LoadS*ls= (LoadS*)ud;
(void)L;
if(ls->size==0)
return NULL;
*size= ls->size;
ls->size= 0;
return ls->s;
}

/*:4*//*5:*/
#line 73 "./luastuff.w"

static void*my_luaalloc(void*ud,void*ptr,size_t osize,size_t nsize)
{
void*ret= NULL;
(void)ud;
if(nsize==0)
free(ptr);
else
ret= realloc(ptr,nsize);
luastate_bytes+= (int)(nsize-osize);
return ret;
}

/*:5*//*6:*/
#line 86 "./luastuff.w"

static int my_luapanic(lua_State*L)
{
(void)L;
fprintf(stderr,"PANIC: unprotected error in call to Lua API (%s)\n",
lua_tostring(L,-1));
return 0;
}

/*:6*//*7:*/
#line 95 "./luastuff.w"

static const luaL_Reg lualibs[]= {
{"",luaopen_base},
{"package",luaopen_package},
{"coroutine",luaopen_coroutine},
{"table",luaopen_table},
{"io",open_iolibext},
{"os",luaopen_os},
{"string",luaopen_string},
{"math",luaopen_math},
{"debug",luaopen_debug},
{"unicode",luaopen_unicode},
{"zip",luaopen_zip},
{"bit32",luaopen_bit32},
{"md5",luaopen_md5},
{"lfs",luaopen_lfs},
{"profiler",luaopen_profiler},
{"lpeg",luaopen_lpeg},
{NULL,NULL}
};


/*:7*//*8:*/
#line 117 "./luastuff.w"

static void do_openlibs(lua_State*L)
{
const luaL_Reg*lib;
for(lib= lualibs;lib->func;lib++){
luaL_requiref(L,lib->name,lib->func,1);
lua_pop(L,1);
}
}

/*:8*//*9:*/
#line 127 "./luastuff.w"

static int load_aux(lua_State*L,int status){
if(status==0)
return 1;
else{
lua_pushnil(L);
lua_insert(L,-2);
return 2;
}
}

/*:9*//*10:*/
#line 138 "./luastuff.w"

static int luatex_loadfile(lua_State*L){
int status= 0;
const char*fname= luaL_optstring(L,1,NULL);
const char*mode= luaL_optstring(L,2,NULL);
int env= !lua_isnone(L,3);
if(!lua_only&&!fname&&interaction==batch_mode){
lua_pushnil(L);
lua_pushstring(L,"reading from stdin is disabled in batch mode");
return 2;
}
status= luaL_loadfilex(L,fname,mode);
if(status==LUA_OK&&env){
lua_pushvalue(L,3);
lua_setupvalue(L,-2,1);
}
return load_aux(L,status);
}

/*:10*//*11:*/
#line 157 "./luastuff.w"

static int luatex_dofile(lua_State*L){
const char*fname= luaL_optstring(L,1,NULL);
int n= lua_gettop(L);
if(!lua_only&&!fname){
if(interaction==batch_mode){
lua_pushnil(L);
lua_pushstring(L,"reading from stdin is disabled in batch mode");
return 2;
}else{
tprint_nl("lua> ");
}
}
if(luaL_loadfile(L,fname)!=0)lua_error(L);
lua_call(L,0,LUA_MULTRET);
return lua_gettop(L)-n;
}


/*:11*//*12:*/
#line 176 "./luastuff.w"

void luainterpreter(void)
{
lua_State*L;
L= lua_newstate(my_luaalloc,NULL);
if(L==NULL){
fprintf(stderr,"Can't create the Lua state.\n");
return;
}
lua_atpanic(L,&my_luapanic);

do_openlibs(L);

lua_pushcfunction(L,luatex_dofile);
lua_setglobal(L,"dofile");
lua_pushcfunction(L,luatex_loadfile);
lua_setglobal(L,"loadfile");

luatex_md5_lua_open(L);

open_oslibext(L,safer_option);



open_strlibext(L);
open_lfslibext(L);







if(!nosocket_option){
lua_getglobal(L,"package");
lua_getfield(L,-1,"loaded");
if(!lua_istable(L,-1)){
lua_newtable(L);
lua_setfield(L,-2,"loaded");
lua_getfield(L,-1,"loaded");
}
luaopen_socket_core(L);
lua_setfield(L,-2,"socket.core");
lua_pushnil(L);
lua_setfield(L,-2,"socket");

luaopen_mime_core(L);
lua_setfield(L,-2,"mime.core");
lua_pushnil(L);
lua_setfield(L,-2,"mime");
lua_pop(L,2);

luatex_socketlua_open(L);
}

luaopen_zlib(L);
lua_setglobal(L,"zlib");
luaopen_gzip(L);


luaopen_ff(L);
luaopen_tex(L);
luaopen_token(L);
luaopen_node(L);
luaopen_texio(L);
luaopen_kpse(L);
luaopen_callback(L);
luaopen_lua(L,startup_filename);
luaopen_stats(L);
luaopen_font(L);
luaopen_lang(L);
luaopen_mplib(L);
luaopen_vf(L);



lua_pushcfunction(L,luaopen_pdf);
lua_pushstring(L,"pdf");
lua_call(L,1,0);

luaL_requiref(L,"img",luaopen_img,1);
lua_pop(L,1);

luaL_requiref(L,"epdf",luaopen_epdf,1);
lua_pop(L,1);


lua_pushcfunction(L,luaopen_pdfscanner);
lua_pushstring(L,"pdfscanner");
lua_call(L,1,0);

lua_createtable(L,0,0);
lua_setglobal(L,"texconfig");

if(safer_option){

(void)hide_lua_value(L,"os","execute");
(void)hide_lua_value(L,"os","rename");
(void)hide_lua_value(L,"os","remove");
(void)hide_lua_value(L,"io","popen");

luaL_checkstack(L,2,"out of stack space");
lua_getglobal(L,"io");
lua_getfield(L,-1,"open_ro");
lua_setfield(L,-2,"open");
(void)hide_lua_value(L,"io","tmpfile");
(void)hide_lua_value(L,"io","output");
(void)hide_lua_value(L,"lfs","chdir");
(void)hide_lua_value(L,"lfs","lock");
(void)hide_lua_value(L,"lfs","touch");
(void)hide_lua_value(L,"lfs","rmdir");
(void)hide_lua_value(L,"lfs","mkdir");
}
Luas= L;
}

/*:12*//*13:*/
#line 292 "./luastuff.w"

int hide_lua_table(lua_State*L,const char*name)
{
int r= 0;
lua_getglobal(L,name);
if(lua_istable(L,-1)){
r= luaL_ref(L,LUA_REGISTRYINDEX);
lua_pushnil(L);
lua_setglobal(L,name);
}
return r;
}

/*:13*//*14:*/
#line 305 "./luastuff.w"

void unhide_lua_table(lua_State*L,const char*name,int r)
{
lua_rawgeti(L,LUA_REGISTRYINDEX,r);
lua_setglobal(L,name);
luaL_unref(L,LUA_REGISTRYINDEX,r);
}

/*:14*//*15:*/
#line 313 "./luastuff.w"

int hide_lua_value(lua_State*L,const char*name,const char*item)
{
int r= 0;
lua_getglobal(L,name);
if(lua_istable(L,-1)){
lua_getfield(L,-1,item);
r= luaL_ref(L,LUA_REGISTRYINDEX);
lua_pushnil(L);
lua_setfield(L,-2,item);
}
return r;
}

/*:15*//*16:*/
#line 327 "./luastuff.w"

void unhide_lua_value(lua_State*L,const char*name,const char*item,int r)
{
lua_getglobal(L,name);
if(lua_istable(L,-1)){
lua_rawgeti(L,LUA_REGISTRYINDEX,r);
lua_setfield(L,-2,item);
luaL_unref(L,LUA_REGISTRYINDEX,r);
}
}


/*:16*//*17:*/
#line 339 "./luastuff.w"

int lua_traceback(lua_State*L)
{
lua_getglobal(L,"debug");
if(!lua_istable(L,-1)){
lua_pop(L,1);
return 1;
}
lua_getfield(L,-1,"traceback");
if(!lua_isfunction(L,-1)){
lua_pop(L,2);
return 1;
}
lua_pushvalue(L,1);
lua_pushinteger(L,2);
lua_call(L,2,1);
return 1;
}

/*:17*//*18:*/
#line 358 "./luastuff.w"

static void luacall(int p,int nameptr,boolean is_string)
{
LoadS ls;
int i;
size_t ll= 0;
char*lua_id;
char*s= NULL;

if(Luas==NULL){
luainterpreter();
}
lua_active++;
if(is_string){
const char*ss= NULL;
lua_rawgeti(Luas,LUA_REGISTRYINDEX,p);
ss= lua_tolstring(Luas,-1,&ll);
s= xmalloc(ll+1);
memcpy(s,ss,ll+1);
lua_pop(Luas,1);
}else{
int l= 0;
s= tokenlist_to_cstring(p,1,&l);
ll= (size_t)l;
}
ls.s= s;
ls.size= ll;
if(ls.size> 0){
if(nameptr> 0){
int l= 0;
lua_id= tokenlist_to_cstring(nameptr,1,&l);
}else if(nameptr<0){
char*tmp= get_lua_name((nameptr+65536));
if(tmp!=NULL)
lua_id= xstrdup(tmp);
else
lua_id= xstrdup("\\latelua ");
}else{
lua_id= xmalloc(20);
snprintf((char*)lua_id,20,"\\latelua ");
}

i= lua_load(Luas,getS,&ls,lua_id,NULL);
if(i!=0){
Luas= luatex_error(Luas,(i==LUA_ERRSYNTAX?0:1));
}else{
int base= lua_gettop(Luas);
lua_checkstack(Luas,1);
lua_pushcfunction(Luas,lua_traceback);
lua_insert(Luas,base);
i= lua_pcall(Luas,0,0,base);
lua_remove(Luas,base);
if(i!=0){
lua_gc(Luas,LUA_GCCOLLECT,0);
Luas= luatex_error(Luas,(i==LUA_ERRRUN?0:1));
}
}
xfree(lua_id);
xfree(ls.s);
}
lua_active--;
}

/*:18*//*19:*/
#line 421 "./luastuff.w"

void late_lua(PDF pdf,halfword p)
{
(void)pdf;
if(late_lua_type(p)==normal){
expand_macros_in_tokenlist(p);
luacall(def_ref,late_lua_name(p),false);
flush_list(def_ref);
}else{
luacall(late_lua_data(p),late_lua_name(p),true);
}
}

/*:19*//*20:*/
#line 434 "./luastuff.w"

void luatokencall(int p,int nameptr)
{
LoadS ls;
int i,l;
char*s= NULL;
char*lua_id;
assert(Luas);
l= 0;
lua_active++;
s= tokenlist_to_cstring(p,1,&l);
ls.s= s;
ls.size= (size_t)l;
if(ls.size> 0){
if(nameptr> 0){
lua_id= tokenlist_to_cstring(nameptr,1,&l);
}else if(nameptr<0){
char*tmp= get_lua_name((nameptr+65536));
if(tmp!=NULL)
lua_id= xstrdup(tmp);
else
lua_id= xstrdup("\\directlua ");
}else{
lua_id= xstrdup("\\directlua ");
}
i= lua_load(Luas,getS,&ls,lua_id,NULL);
xfree(s);
if(i!=0){
Luas= luatex_error(Luas,(i==LUA_ERRSYNTAX?0:1));
}else{
int base= lua_gettop(Luas);
lua_checkstack(Luas,1);
lua_pushcfunction(Luas,lua_traceback);
lua_insert(Luas,base);
i= lua_pcall(Luas,0,0,base);
lua_remove(Luas,base);
if(i!=0){
lua_gc(Luas,LUA_GCCOLLECT,0);
Luas= luatex_error(Luas,(i==LUA_ERRRUN?0:1));
}
}
xfree(lua_id);
}
lua_active--;
}

/*:20*//*21:*/
#line 480 "./luastuff.w"

lua_State*luatex_error(lua_State*L,int is_fatal)
{

const_lstring luaerr;
char*err= NULL;
if(lua_isstring(L,-1)){
luaerr.s= lua_tolstring(L,-1,&luaerr.l);
err= (char*)xmalloc((unsigned)(luaerr.l+1));
snprintf(err,(luaerr.l+1),"%s",luaerr.s);
}
if(is_fatal> 0){



lua_fatal_error(err);

xfree(err);
lua_close(L);
return(lua_State*)NULL;
}else{
lua_norm_error(err);
xfree(err);
return L;
}
}

/*:21*//*22:*/
#line 507 "./luastuff.w"

void preset_environment(lua_State*L,const parm_struct*p,const char*s)
{
int i;
assert(L!=NULL);

lua_pushstring(L,s);
lua_gettable(L,LUA_REGISTRYINDEX);
assert(lua_isnil(L,-1));
lua_pop(L,1);
lua_pushstring(L,s);
lua_newtable(L);
for(i= 1,++p;p->name!=NULL;i++,p++){
assert(i==p->idx);
lua_pushstring(L,p->name);
lua_pushinteger(L,p->idx);
lua_settable(L,-3);
}
lua_settable(L,LUA_REGISTRYINDEX);
}/*:22*/
