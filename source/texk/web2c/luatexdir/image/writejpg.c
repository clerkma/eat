/*1:*/
#line 21 "./writejpg.w"

static const char _svn_version[]= 
"$Id: writejpg.w 4442 2012-05-25 22:40:34Z hhenkel $"
"$URL: https://foundry.supelec.fr/svn/luatex/trunk/source/texk/web2c/luatexdir/image/writejpg.w $";

#include <assert.h> 
#include "ptexlib.h"
#include "image/image.h"
#include "image/writejpg.h"

/*:1*//*2:*/
#line 31 "./writejpg.w"

#define JPG_GRAY  1             
#define JPG_RGB   3             
#define JPG_CMYK  4             

typedef enum{
M_SOF0= 0xc0,
M_SOF1= 0xc1,
M_SOF2= 0xc2,
M_SOF3= 0xc3,

M_SOF5= 0xc5,
M_SOF6= 0xc6,
M_SOF7= 0xc7,

M_JPG= 0xc8,
M_SOF9= 0xc9,
M_SOF10= 0xca,
M_SOF11= 0xcb,

M_SOF13= 0xcd,
M_SOF14= 0xce,
M_SOF15= 0xcf,

M_DHT= 0xc4,

M_DAC= 0xcc,

M_RST0= 0xd0,
M_RST1= 0xd1,
M_RST2= 0xd2,
M_RST3= 0xd3,
M_RST4= 0xd4,
M_RST5= 0xd5,
M_RST6= 0xd6,
M_RST7= 0xd7,

M_SOI= 0xd8,
M_EOI= 0xd9,
M_SOS= 0xda,
M_DQT= 0xdb,
M_DNL= 0xdc,
M_DRI= 0xdd,
M_DHP= 0xde,
M_EXP= 0xdf,

M_APP0= 0xe0,
M_APP1= 0xe1,
M_APP2= 0xe2,
M_APP3= 0xe3,
M_APP4= 0xe4,
M_APP5= 0xe5,
M_APP6= 0xe6,
M_APP7= 0xe7,
M_APP8= 0xe8,
M_APP9= 0xe9,
M_APP10= 0xea,
M_APP11= 0xeb,
M_APP12= 0xec,
M_APP13= 0xed,
M_APP14= 0xee,
M_APP15= 0xef,

M_JPG0= 0xf0,
M_JPG13= 0xfd,
M_COM= 0xfe,

M_TEM= 0x01,

M_ERROR= 0x100
}JPEG_MARKER;

/*:2*//*3:*/
#line 103 "./writejpg.w"

static void close_and_cleanup_jpg(image_dict*idict)
{
assert(idict!=NULL);
assert(img_file(idict)!=NULL);
assert(img_filepath(idict)!=NULL);
xfclose(img_file(idict),img_filepath(idict));
img_file(idict)= NULL;
assert(img_jpg_ptr(idict)!=NULL);
xfree(img_jpg_ptr(idict));
}

/*:3*//*4:*/
#line 115 "./writejpg.w"

void read_jpg_info(PDF pdf,image_dict*idict,img_readtype_e readtype)
{
int i,units= 0;
unsigned char jpg_id[]= "JFIF";
assert(idict!=NULL);
assert(img_type(idict)==IMG_TYPE_JPG);
img_totalpages(idict)= 1;
img_pagenum(idict)= 1;
img_xres(idict)= img_yres(idict)= 0;
assert(img_file(idict)==NULL);
img_file(idict)= xfopen(img_filepath(idict),FOPEN_RBIN_MODE);
assert(img_jpg_ptr(idict)==NULL);
img_jpg_ptr(idict)= xtalloc(1,jpg_img_struct);
xfseek(img_file(idict),0,SEEK_END,img_filepath(idict));
img_jpg_ptr(idict)->length= xftell(img_file(idict),img_filepath(idict));
xfseek(img_file(idict),0,SEEK_SET,img_filepath(idict));
if((unsigned int)read2bytes(img_file(idict))!=0xFFD8)
pdftex_fail("reading JPEG image failed (no JPEG header found)");

if((unsigned int)read2bytes(img_file(idict))==0xFFE0){
(void)read2bytes(img_file(idict));
for(i= 0;i<5;i++){
if(xgetc(img_file(idict))!=jpg_id[i])
break;
}
if(i==5){
(void)read2bytes(img_file(idict));
units= xgetc(img_file(idict));
img_xres(idict)= (int)read2bytes(img_file(idict));
img_yres(idict)= (int)read2bytes(img_file(idict));
switch(units){
case 1:
break;
case 2:
img_xres(idict)= (int)((double)img_xres(idict)*2.54);
img_yres(idict)= (int)((double)img_yres(idict)*2.54);
break;
default:
img_xres(idict)= img_yres(idict)= 0;
break;
}
}

if((img_xres(idict)==0)&&(img_yres(idict)!=0)){
img_xres(idict)= img_yres(idict);
}
if((img_yres(idict)==0)&&(img_xres(idict)!=0)){
img_yres(idict)= img_xres(idict);
}
}
xfseek(img_file(idict),0,SEEK_SET,img_filepath(idict));
while(1){
if(feof(img_file(idict)))
pdftex_fail("reading JPEG image failed (premature file end)");
if(fgetc(img_file(idict))!=0xFF)
pdftex_fail("reading JPEG image failed (no marker found)");
i= xgetc(img_file(idict));
switch(i){
case M_SOF3:
case M_SOF5:
case M_SOF6:
case M_SOF7:
case M_SOF9:
case M_SOF10:
case M_SOF11:
case M_SOF13:
case M_SOF14:
case M_SOF15:
pdftex_fail("unsupported type of compression (SOF_%d)",i-M_SOF0);
break;
case M_SOF2:
if(pdf->minor_version<=2)
pdftex_fail("cannot use progressive DCT with PDF-1.2");
case M_SOF0:
case M_SOF1:
(void)read2bytes(img_file(idict));
img_colordepth(idict)= xgetc(img_file(idict));
img_ysize(idict)= (int)read2bytes(img_file(idict));
img_xsize(idict)= (int)read2bytes(img_file(idict));
img_jpg_color(idict)= xgetc(img_file(idict));
xfseek(img_file(idict),0,SEEK_SET,img_filepath(idict));
switch(img_jpg_color(idict)){
case JPG_GRAY:
img_procset(idict)|= PROCSET_IMAGE_B;
break;
case JPG_RGB:
img_procset(idict)|= PROCSET_IMAGE_C;
break;
case JPG_CMYK:
img_procset(idict)|= PROCSET_IMAGE_C;
break;
default:
pdftex_fail("Unsupported color space %i",
(int)img_jpg_color(idict));
}
if(readtype==IMG_CLOSEINBETWEEN)
close_and_cleanup_jpg(idict);
return;
case M_SOI:
case M_EOI:
case M_TEM:
case M_RST0:
case M_RST1:
case M_RST2:
case M_RST3:
case M_RST4:
case M_RST5:
case M_RST6:
case M_RST7:
break;
default:
xfseek(img_file(idict),(int)read2bytes(img_file(idict))-2,
SEEK_CUR,img_filepath(idict));
break;
}
}
assert(0);
}

/*:4*//*5:*/
#line 235 "./writejpg.w"

static void reopen_jpg(PDF pdf,image_dict*idict)
{
int width,height,xres,yres;
width= img_xsize(idict);
height= img_ysize(idict);
xres= img_xres(idict);
yres= img_yres(idict);
read_jpg_info(pdf,idict,IMG_KEEPOPEN);
if(width!=img_xsize(idict)||height!=img_ysize(idict)
||xres!=img_xres(idict)||yres!=img_yres(idict))
pdftex_fail("writejpg: image dimensions have changed");
}

/*:5*//*6:*/
#line 249 "./writejpg.w"

void write_jpg(PDF pdf,image_dict*idict)
{
size_t l;
assert(idict!=NULL);
if(img_file(idict)==NULL)
reopen_jpg(pdf,idict);
assert(img_jpg_ptr(idict)!=NULL);
pdf_begin_obj(pdf,img_objnum(idict),OBJSTM_NEVER);
pdf_begin_dict(pdf);
pdf_dict_add_name(pdf,"Type","XObject");
pdf_dict_add_name(pdf,"Subtype","Image");
pdf_dict_add_img_filename(pdf,idict);
if(img_attr(idict)!=NULL&&strlen(img_attr(idict))> 0)
pdf_printf(pdf,"\n%s\n",img_attr(idict));
pdf_dict_add_int(pdf,"Width",(int)img_xsize(idict));
pdf_dict_add_int(pdf,"Height",(int)img_ysize(idict));
pdf_dict_add_int(pdf,"BitsPerComponent",(int)img_colordepth(idict));
pdf_dict_add_int(pdf,"Length",(int)img_jpg_ptr(idict)->length);
if(img_colorspace(idict)!=0){
pdf_dict_add_ref(pdf,"ColorSpace",(int)img_colorspace(idict));
}else{
switch(img_jpg_color(idict)){
case JPG_GRAY:
pdf_dict_add_name(pdf,"ColorSpace","DeviceGray");
break;
case JPG_RGB:
pdf_dict_add_name(pdf,"ColorSpace","DeviceRGB");
break;
case JPG_CMYK:
pdf_dict_add_name(pdf,"ColorSpace","DeviceCMYK");
pdf_add_name(pdf,"Decode");
pdf_begin_array(pdf);
pdf_add_int(pdf,1);
pdf_add_int(pdf,0);
pdf_add_int(pdf,1);
pdf_add_int(pdf,0);
pdf_add_int(pdf,1);
pdf_add_int(pdf,0);
pdf_add_int(pdf,1);
pdf_add_int(pdf,0);
pdf_end_array(pdf);
break;
default:
pdftex_fail("Unsupported color space %i",
(int)img_jpg_color(idict));
}
}
pdf_dict_add_name(pdf,"Filter","DCTDecode");
pdf_end_dict(pdf);
pdf_begin_stream(pdf);
assert(pdf->zip_write_state==NO_ZIP);
l= (size_t)img_jpg_ptr(idict)->length;
xfseek(img_file(idict),0,SEEK_SET,img_filepath(idict));
if(read_file_to_buf(pdf,img_file(idict),l)!=l)
pdftex_fail("writejpg: fread failed");
pdf_end_stream(pdf);
pdf_end_obj(pdf);
close_and_cleanup_jpg(idict);
}/*:6*/
