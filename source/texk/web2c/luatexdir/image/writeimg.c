/*2:*/
#line 23 "./writeimg.w"

static const char _svn_version[]= 
"$Id: writeimg.w 4524 2012-12-20 15:38:02Z taco $"
"$URL: https://foundry.supelec.fr/svn/luatex/trunk/source/texk/web2c/luatexdir/image/writeimg.w $";

#include "ptexlib.h"
#include <assert.h> 
#include <kpathsea/c-auto.h> 
#include <kpathsea/c-memstr.h> 

/*:2*//*3:*/
#line 33 "./writeimg.w"

#include "image/image.h"
#include "image/writejpg.h"
#include "image/writejp2.h"
#include "image/writepng.h"
#include "image/writejbig2.h"

#include "lua52/lua.h"          
#include "lua52/lauxlib.h"

/*:3*//*4:*/
#line 43 "./writeimg.w"

#define pdf_image_resolution int_par(pdf_image_resolution_code)
#define pdf_pagebox int_par(pdf_pagebox_code)

/*:4*//*5:*/
#line 122 "./writeimg.w"

#define HEADER_JPG "\xFF\xD8"
#define HEADER_PNG "\x89PNG\r\n\x1A\n"
#define HEADER_JBIG2 "\x97\x4A\x42\x32\x0D\x0A\x1A\x0A"
#define HEADER_JP2 "\x6A\x50\x20\x20"
#define HEADER_PDF "%PDF-1."
#define MAX_HEADER (sizeof(HEADER_PNG)-1)

static void check_type_by_header(image_dict*idict)
{
int i;
FILE*file= NULL;
char header[MAX_HEADER];

assert(idict!=NULL);
if(img_type(idict)!=IMG_TYPE_NONE)
return;

file= xfopen(img_filepath(idict),FOPEN_RBIN_MODE);
for(i= 0;(unsigned)i<MAX_HEADER;i++){
header[i]= (char)xgetc(file);
if(feof(file))
pdftex_fail("reading image file failed");
}
xfclose(file,img_filepath(idict));

if(strncmp(header,HEADER_JPG,sizeof(HEADER_JPG)-1)==0)
img_type(idict)= IMG_TYPE_JPG;
else if(strncmp(header+4,HEADER_JP2,sizeof(HEADER_JP2)-1)==0)
img_type(idict)= IMG_TYPE_JP2;
else if(strncmp(header,HEADER_PNG,sizeof(HEADER_PNG)-1)==0)
img_type(idict)= IMG_TYPE_PNG;
else if(strncmp(header,HEADER_JBIG2,sizeof(HEADER_JBIG2)-1)==0)
img_type(idict)= IMG_TYPE_JBIG2;
else if(strncmp(header,HEADER_PDF,sizeof(HEADER_PDF)-1)==0)
img_type(idict)= IMG_TYPE_PDF;
}

/*:5*//*6:*/
#line 160 "./writeimg.w"

static void check_type_by_extension(image_dict*idict)
{
char*image_suffix;

assert(idict!=NULL);
if(img_type(idict)!=IMG_TYPE_NONE)
return;

if((image_suffix= strrchr(img_filename(idict),'.'))==0)
img_type(idict)= IMG_TYPE_NONE;
else if(strcasecmp(image_suffix,".png")==0)
img_type(idict)= IMG_TYPE_PNG;
else if(strcasecmp(image_suffix,".jpg")==0||
strcasecmp(image_suffix,".jpeg")==0)
img_type(idict)= IMG_TYPE_JPG;
else if(strcasecmp(image_suffix,".jp2")==0)
img_type(idict)= IMG_TYPE_JP2;
else if(strcasecmp(image_suffix,".jbig2")==0||
strcasecmp(image_suffix,".jb2")==0)
img_type(idict)= IMG_TYPE_JBIG2;
else if(strcasecmp(image_suffix,".pdf")==0)
img_type(idict)= IMG_TYPE_PDF;
}

/*:6*//*7:*/
#line 185 "./writeimg.w"

void new_img_pdfstream_struct(image_dict*p)
{
assert(p!=NULL);
assert(img_pdfstream_ptr(p)==NULL);
img_pdfstream_ptr(p)= xtalloc(1,pdf_stream_struct);
img_pdfstream_stream(p)= NULL;
}

/*:7*//*8:*/
#line 194 "./writeimg.w"

static void init_image(image*p)
{
assert(p!=NULL);
set_wd_running(p);
set_ht_running(p);
set_dp_running(p);
img_transform(p)= 0;
img_dict(p)= NULL;
img_dictref(p)= LUA_NOREF;
}

/*:8*//*9:*/
#line 206 "./writeimg.w"

image*new_image(void)
{
image*p= xtalloc(1,image);
init_image(p);
return p;
}

/*:9*//*10:*/
#line 214 "./writeimg.w"

static void init_image_dict(image_dict*p)
{
assert(p!=NULL);
memset(p,0,sizeof(image_dict));
set_wd_running(p);
set_ht_running(p);
set_dp_running(p);
img_transform(p)= 0;
img_pagenum(p)= 1;
img_type(p)= IMG_TYPE_NONE;
img_pagebox(p)= PDF_BOX_SPEC_MEDIA;
img_unset_bbox(p);
img_unset_group(p);
img_state(p)= DICT_NEW;
img_index(p)= -1;
}

/*:10*//*11:*/
#line 232 "./writeimg.w"

image_dict*new_image_dict(void)
{
image_dict*p= xtalloc(1,image_dict);
init_image_dict(p);
return p;
}

/*:11*//*12:*/
#line 240 "./writeimg.w"

static void free_dict_strings(image_dict*p)
{
xfree(img_filename(p));
xfree(img_filepath(p));
xfree(img_attr(p));
xfree(img_pagename(p));
}

/*:12*//*13:*/
#line 249 "./writeimg.w"

void free_image_dict(image_dict*p)
{
if(ini_version)
return;

assert(img_state(p)<DICT_REFERED);
switch(img_type(p)){
case IMG_TYPE_PDF:
unrefPdfDocument(img_filepath(p));
break;
case IMG_TYPE_PNG:
assert(img_png_ptr(p)==NULL);
break;
case IMG_TYPE_JPG:
assert(img_jpg_ptr(p)==NULL);
break;
case IMG_TYPE_JP2:
assert(img_jp2_ptr(p)==NULL);
break;
case IMG_TYPE_JBIG2:
break;
case IMG_TYPE_PDFSTREAM:
if(img_pdfstream_ptr(p)!=NULL){
xfree(img_pdfstream_stream(p));
xfree(img_pdfstream_ptr(p));
}
break;
case IMG_TYPE_NONE:
break;
default:
assert(0);
}
free_dict_strings(p);
assert(img_file(p)==NULL);
xfree(p);
}

/*:13*//*14:*/
#line 287 "./writeimg.w"

void read_img(PDF pdf,
image_dict*idict,int minor_version,int inclusion_errorlevel)
{
char*filepath= NULL;
int callback_id;
assert(idict!=NULL);
if(img_filename(idict)==NULL)
pdftex_fail("image file name missing");
callback_id= callback_defined(find_image_file_callback);
if(img_filepath(idict)==NULL){
if(callback_id> 0
&&run_callback(callback_id,"S->S",img_filename(idict),
&filepath)){
if(filepath&&(strlen(filepath)> 0))
img_filepath(idict)= strdup(filepath);
}else
img_filepath(idict)= 
kpse_find_file(img_filename(idict),kpse_tex_format,true);
if(img_filepath(idict)==NULL)
pdftex_fail("cannot find image file '%s'",img_filename(idict));
}
recorder_record_input(img_filename(idict));

check_type_by_header(idict);
check_type_by_extension(idict);

switch(img_type(idict)){
case IMG_TYPE_PDF:
assert(pdf!=NULL);
read_pdf_info(idict,minor_version,inclusion_errorlevel,
IMG_CLOSEINBETWEEN);
break;
case IMG_TYPE_PNG:
read_png_info(idict,IMG_CLOSEINBETWEEN);
break;
case IMG_TYPE_JPG:
read_jpg_info(pdf,idict,IMG_CLOSEINBETWEEN);
break;
case IMG_TYPE_JP2:
read_jp2_info(idict,IMG_CLOSEINBETWEEN);
break;
case IMG_TYPE_JBIG2:
if(minor_version<4){
pdftex_fail
("JBIG2 images only possible with at least PDF 1.4; you are generating PDF 1.%i",
(int)minor_version);
}
read_jbig2_info(idict);
break;
default:
pdftex_fail("internal error: unknown image type (2)");
}
cur_file_name= NULL;
if(img_state(idict)<DICT_FILESCANNED)
img_state(idict)= DICT_FILESCANNED;
}

/*:14*//*15:*/
#line 345 "./writeimg.w"

static image_dict*read_image(PDF pdf,char*file_name,int page_num,
char*page_name,int colorspace,
int page_box,int minor_version,
int inclusion_errorlevel)
{
image*a= new_image();
image_dict*idict= img_dict(a)= new_image_dict();
pdf->ximage_count++;
img_objnum(idict)= pdf_create_obj(pdf,obj_type_ximage,pdf->ximage_count);
img_index(idict)= pdf->ximage_count;
set_obj_data_ptr(pdf,img_objnum(idict),img_index(idict));
idict_to_array(idict);
img_colorspace(idict)= colorspace;
img_pagenum(idict)= page_num;
img_pagename(idict)= page_name;
assert(file_name!=NULL);
cur_file_name= file_name;
img_filename(idict)= file_name;
img_pagebox(idict)= page_box;
read_img(pdf,idict,minor_version,inclusion_errorlevel);
return idict;
}

/*:15*//*16:*/
#line 370 "./writeimg.w"

static pdfboxspec_e scan_pdf_box_spec(void)
{
if(scan_keyword("mediabox"))
return PDF_BOX_SPEC_MEDIA;
else if(scan_keyword("cropbox"))
return PDF_BOX_SPEC_CROP;
else if(scan_keyword("bleedbox"))
return PDF_BOX_SPEC_BLEED;
else if(scan_keyword("trimbox"))
return PDF_BOX_SPEC_TRIM;
else if(scan_keyword("artbox"))
return PDF_BOX_SPEC_ART;
return PDF_BOX_SPEC_NONE;
}

/*:16*//*17:*/
#line 386 "./writeimg.w"

void scan_pdfximage(PDF pdf)
{
scaled_whd alt_rule;
image_dict*idict;
int transform= 0,page= 1,pagebox,colorspace= 0;
char*named= NULL,*attr= NULL,*file_name= NULL;
alt_rule= scan_alt_rule();
if(scan_keyword("attr")){
scan_pdf_ext_toks();
attr= tokenlist_to_cstring(def_ref,true,NULL);
delete_token_ref(def_ref);
}
if(scan_keyword("named")){
scan_pdf_ext_toks();
named= tokenlist_to_cstring(def_ref,true,NULL);
delete_token_ref(def_ref);
page= 0;
}else if(scan_keyword("page")){
scan_int();
page= cur_val;
}
if(scan_keyword("colorspace")){
scan_int();
colorspace= cur_val;
}
pagebox= scan_pdf_box_spec();
if(pagebox==PDF_BOX_SPEC_NONE){
pagebox= pdf_pagebox;
if(pagebox==PDF_BOX_SPEC_NONE)
pagebox= PDF_BOX_SPEC_CROP;
}
scan_pdf_ext_toks();
file_name= tokenlist_to_cstring(def_ref,true,NULL);
assert(file_name!=NULL);
delete_token_ref(def_ref);
idict= 
read_image(pdf,file_name,page,named,colorspace,pagebox,
pdf_minor_version,pdf_inclusion_errorlevel);
img_attr(idict)= attr;
img_dimen(idict)= alt_rule;
img_transform(idict)= transform;
pdf_last_ximage= img_objnum(idict);
pdf_last_ximage_pages= img_totalpages(idict);
pdf_last_ximage_colordepth= img_colordepth(idict);
}

/*:17*//*18:*/
#line 433 "./writeimg.w"

#define tail          cur_list.tail_field

void scan_pdfrefximage(PDF pdf)
{
int transform= 0;
image_dict*idict;
scaled_whd alt_rule,dim;
alt_rule= scan_alt_rule();
scan_int();
check_obj_type(pdf,obj_type_ximage,cur_val);
new_whatsit(pdf_refximage_node);
idict= idict_array[obj_data_ptr(pdf,cur_val)];
if(alt_rule.wd!=null_flag||alt_rule.ht!=null_flag
||alt_rule.dp!=null_flag)
dim= scale_img(idict,alt_rule,transform);
else
dim= scale_img(idict,img_dimen(idict),img_transform(idict));
width(tail)= dim.wd;
height(tail)= dim.ht;
depth(tail)= dim.dp;
pdf_ximage_transform(tail)= transform;
pdf_ximage_index(tail)= img_index(idict);
}

/*:18*//*19:*/
#line 471 "./writeimg.w"

scaled_whd tex_scale(scaled_whd nat,scaled_whd tex)
{
scaled_whd res;
if(!is_running(tex.wd)&&!is_running(tex.ht)&&!is_running(tex.dp)){

res= tex;
}else if(!is_running(tex.wd)){
res.wd= tex.wd;
if(!is_running(tex.ht)){
res.ht= tex.ht;

res.dp= ext_xn_over_d(tex.ht,nat.dp,nat.ht);
}else if(!is_running(tex.dp)){
res.dp= tex.dp;

res.ht= ext_xn_over_d(tex.wd,nat.ht+nat.dp,nat.wd)-tex.dp;
}else{

res.ht= ext_xn_over_d(tex.wd,nat.ht,nat.wd);
res.dp= ext_xn_over_d(tex.wd,nat.dp,nat.wd);
}
}else if(!is_running(tex.ht)){
res.ht= tex.ht;
if(!is_running(tex.dp)){
res.dp= tex.dp;

res.wd= ext_xn_over_d(tex.ht+tex.dp,nat.wd,nat.ht+nat.dp);
}else{

res.wd= ext_xn_over_d(tex.ht,nat.wd,nat.ht);
res.dp= ext_xn_over_d(tex.ht,nat.dp,nat.ht);
}
}else if(!is_running(tex.dp)){
res.dp= tex.dp;

res.ht= nat.ht-(tex.dp-nat.dp);
res.wd= nat.wd;
}else{

res= nat;
}
return res;
}

/*:19*//*20:*/
#line 521 "./writeimg.w"

scaled_whd scale_img(image_dict*idict,scaled_whd alt_rule,int transform)
{
int x,y,xr,yr,tmp;
scaled_whd nat;
int default_res;
assert(idict!=NULL);
if((img_type(idict)==IMG_TYPE_PDF
||img_type(idict)==IMG_TYPE_PDFSTREAM)&&img_is_bbox(idict)){
x= img_xsize(idict)= img_bbox(idict)[2]-img_bbox(idict)[0];
y= img_ysize(idict)= img_bbox(idict)[3]-img_bbox(idict)[1];
img_xorig(idict)= img_bbox(idict)[0];
img_yorig(idict)= img_bbox(idict)[1];
}else{
x= img_xsize(idict);
y= img_ysize(idict);
}
xr= img_xres(idict);
yr= img_yres(idict);
if(x<=0||y<=0||xr<0||yr<0)
pdftex_fail("ext1: invalid image dimensions");
if(xr> 65535||yr> 65535){
xr= 0;
yr= 0;
pdftex_warn("ext1: too large image resolution ignored");
}
if(((transform-img_rotation(idict))&1)==1){
tmp= x;
x= y;
y= tmp;
tmp= xr;
xr= yr;
yr= tmp;
}
nat.dp= 0;
if(img_type(idict)==IMG_TYPE_PDF
||img_type(idict)==IMG_TYPE_PDFSTREAM){
nat.wd= x;
nat.ht= y;
}else{
default_res= fix_int(pdf_image_resolution,0,65535);
if(default_res> 0&&(xr==0||yr==0)){
xr= default_res;
yr= default_res;
}
if(xr> 0&&yr> 0){
nat.wd= ext_xn_over_d(one_hundred_inch,x,100*xr);
nat.ht= ext_xn_over_d(one_hundred_inch,y,100*yr);
}else{
nat.wd= ext_xn_over_d(one_hundred_inch,x,7200);
nat.ht= ext_xn_over_d(one_hundred_inch,y,7200);
}
}
return tex_scale(nat,alt_rule);
}

/*:20*//*21:*/
#line 577 "./writeimg.w"

void write_img(PDF pdf,image_dict*idict)
{
assert(idict!=NULL);
if(img_state(idict)<DICT_WRITTEN){
if(tracefilenames)
tex_printf(" <%s",img_filepath(idict));
switch(img_type(idict)){
case IMG_TYPE_PNG:
write_png(pdf,idict);
break;
case IMG_TYPE_JPG:
write_jpg(pdf,idict);
break;
case IMG_TYPE_JP2:
write_jp2(pdf,idict);
break;
case IMG_TYPE_JBIG2:
write_jbig2(pdf,idict);
break;
case IMG_TYPE_PDF:
write_epdf(pdf,idict);
break;
case IMG_TYPE_PDFSTREAM:
write_pdfstream(pdf,idict);
break;
default:
pdftex_fail("internal error: unknown image type (1)");
}
if(tracefilenames)
tex_printf(">");
if(img_type(idict)==IMG_TYPE_PNG){
write_additional_png_objects(pdf);
}
}
if(img_state(idict)<DICT_WRITTEN)
img_state(idict)= DICT_WRITTEN;
}

/*:21*//*22:*/
#line 617 "./writeimg.w"

void pdf_write_image(PDF pdf,int n)
{
if(pdf->draftmode==0)
write_img(pdf,idict_array[obj_data_ptr(pdf,n)]);
}

/*:22*//*23:*/
#line 624 "./writeimg.w"

void check_pdfstream_dict(image_dict*idict)
{
if(!img_is_bbox(idict))
pdftex_fail("image.stream: no bbox given");
if(img_state(idict)<DICT_FILESCANNED)
img_state(idict)= DICT_FILESCANNED;
}

/*:23*//*24:*/
#line 633 "./writeimg.w"

void write_pdfstream(PDF pdf,image_dict*idict)
{
assert(img_pdfstream_ptr(idict)!=NULL);
assert(img_is_bbox(idict));
pdf_begin_obj(pdf,img_objnum(idict),OBJSTM_NEVER);
pdf_begin_dict(pdf);
pdf_dict_add_name(pdf,"Type","XObject");
pdf_dict_add_name(pdf,"Subtype","Form");
if(img_attr(idict)!=NULL&&strlen(img_attr(idict))> 0)
pdf_printf(pdf,"\n%s\n",img_attr(idict));
pdf_dict_add_int(pdf,"FormType",1);
pdf_add_name(pdf,"BBox");
pdf_begin_array(pdf);
copyReal(pdf,sp2bp(img_bbox(idict)[0]));
copyReal(pdf,sp2bp(img_bbox(idict)[1]));
copyReal(pdf,sp2bp(img_bbox(idict)[2]));
copyReal(pdf,sp2bp(img_bbox(idict)[3]));
pdf_end_array(pdf);
pdf_dict_add_streaminfo(pdf);
pdf_end_dict(pdf);
pdf_begin_stream(pdf);
if(img_pdfstream_stream(idict)!=NULL)
pdf_puts(pdf,img_pdfstream_stream(idict));
pdf_end_stream(pdf);
pdf_end_obj(pdf);
}

/*:24*//*25:*/
#line 661 "./writeimg.w"


idict_entry*idict_ptr,*idict_array= NULL;
size_t idict_limit;

void idict_to_array(image_dict*idict)
{
assert(idict!=NULL);
if(idict_ptr-idict_array==0){
alloc_array(idict,1,SMALL_BUF_SIZE);
idict_ptr++;
}
alloc_array(idict,1,SMALL_BUF_SIZE);
*idict_ptr= idict;
assert(img_index(idict)==idict_ptr-idict_array);
idict_ptr++;
}

void pdf_dict_add_img_filename(PDF pdf,image_dict*idict)
{
char s[21],*p;
assert(idict!=NULL);

if(img_type(idict)!=IMG_TYPE_PDF)
return;
if(img_visiblefilename(idict)!=NULL){
if(strlen(img_visiblefilename(idict))==0)
return;
else
p= img_visiblefilename(idict);
}else
p= img_filepath(idict);

snprintf(s,20,"%s.FileName",pdfkeyprefix);
pdf_add_name(pdf,s);
pdf_printf(pdf," (%s)",convertStringToPDFString(p,strlen(p)));
}

/*:25*//*26:*/
#line 716 "./writeimg.w"

#define dumpinteger generic_dump
#define undumpinteger generic_undump

/*:26*//*27:*/
#line 724 "./writeimg.w"

#define dumpcharptr(a)                          \
  do {                                          \
    int x;                                      \
    if (a!=NULL) {                              \
 x =  (int)strlen(a)+1;   \
      dumpinteger(x);  dump_things(*a, x);      \
    } else {                                    \
      x =  0; dumpinteger(x);                    \
    }                                           \
  } while (0)

#define undumpcharptr(s)                        \
  do {                                          \
    int x;                                      \
    char *a;                                    \
    undumpinteger (x);                          \
    if (x> 0) {                                  \
      a =  xmalloc((unsigned)x);    \
      undump_things(*a,x);                      \
      s =  a ;                                   \
    } else { s =  NULL; }                        \
  } while (0)

/*:27*//*28:*/
#line 748 "./writeimg.w"

void dumpimagemeta(void)
{
int cur_index,i;
image_dict*idict;

i= (int)idict_limit;
dumpinteger(i);
cur_index= (int)(idict_ptr-idict_array);
dumpinteger(cur_index);

for(i= 1;i<cur_index;i++){
idict= idict_array[i];
assert(idict!=NULL);
dumpcharptr(img_filename(idict));
dumpinteger(img_type(idict));
dumpinteger(img_procset(idict));
dumpinteger(img_xsize(idict));
dumpinteger(img_ysize(idict));
dumpinteger(img_xres(idict));
dumpinteger(img_yres(idict));
dumpinteger(img_totalpages(idict));
dumpinteger(img_colorspace(idict));




if(img_type(idict)==IMG_TYPE_PDF){
dumpinteger(img_pagebox(idict));
dumpinteger(img_pagenum(idict));
}else if(img_type(idict)==IMG_TYPE_JBIG2){
dumpinteger(img_pagenum(idict));
}

}
}

/*:28*//*29:*/
#line 785 "./writeimg.w"

void undumpimagemeta(PDF pdf,int pdfversion,int pdfinclusionerrorlevel)
{
int cur_index,i;
image_dict*idict;

assert(pdf!=NULL);
undumpinteger(i);
idict_limit= (size_t)i;

idict_array= xtalloc(idict_limit,idict_entry);
undumpinteger(cur_index);
idict_ptr= idict_array+cur_index;

for(i= 1;i<cur_index;i++){
idict= new_image_dict();
assert(idict!=NULL);
assert(img_index(idict)==-1);
idict_to_array(idict);
undumpcharptr(img_filename(idict));
undumpinteger(img_type(idict));
undumpinteger(img_procset(idict));
undumpinteger(img_xsize(idict));
undumpinteger(img_ysize(idict));
undumpinteger(img_xres(idict));
undumpinteger(img_yres(idict));
undumpinteger(img_totalpages(idict));
undumpinteger(img_colorspace(idict));

switch(img_type(idict)){
case IMG_TYPE_PDF:
undumpinteger(img_pagebox(idict));
undumpinteger(img_pagenum(idict));
break;
case IMG_TYPE_PNG:
case IMG_TYPE_JPG:
case IMG_TYPE_JP2:
break;
case IMG_TYPE_JBIG2:
if(pdfversion<4){
pdftex_fail
("JBIG2 images only possible with at least PDF 1.4; you are generating PDF 1.%i",
(int)pdfversion);
}
undumpinteger(img_pagenum(idict));
break;
default:
pdftex_fail("unknown type of image");
}
read_img(pdf,idict,pdfversion,pdfinclusionerrorlevel);
}
}

/*:29*//*30:*/
#line 839 "./writeimg.w"

scaled_whd scan_alt_rule(void)
{
boolean loop;
scaled_whd alt_rule;
alt_rule.wd= null_flag;
alt_rule.ht= null_flag;
alt_rule.dp= null_flag;
do{
loop= false;
if(scan_keyword("width")){
scan_normal_dimen();
alt_rule.wd= cur_val;
loop= true;
}else if(scan_keyword("height")){
scan_normal_dimen();
alt_rule.ht= cur_val;
loop= true;
}else if(scan_keyword("depth")){
scan_normal_dimen();
alt_rule.dp= cur_val;
loop= true;
}
}while(loop);
return alt_rule;
}

/*:30*//*31:*/
#line 867 "./writeimg.w"

size_t read_file_to_buf(PDF pdf,FILE*f,size_t len)
{
size_t i,j,k= 0;
while(len> 0){
i= (size_t)(len> pdf->buf->size)?(size_t)pdf->buf->size:len;
pdf_room(pdf,(int)i);
j= fread(pdf->buf->p,1,i,f);
pdf->buf->p+= j;
k+= j;
len-= j;
if(i!=j)
break;
}
return k;
}/*:31*/
