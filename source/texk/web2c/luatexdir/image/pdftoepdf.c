/*1:*/
#line 21 "./pdftoepdf.w"

static const char _svn_version[]= 
"$Id: pdftoepdf.w 4479 2012-11-07 16:38:55Z taco $"
"$URL: https://foundry.supelec.fr/svn/luatex/trunk/source/texk/web2c/luatexdir/image/pdftoepdf.w $";


#define __STDC_FORMAT_MACROS 

#include "image/epdf.h"




static GBool isInit= gFalse;




static avl_table*PdfDocumentTree= NULL;



static int CompPdfDocument(const void*pa,const void*pb,void*)
{
return strcmp(((const PdfDocument*)pa)->file_path,
((const PdfDocument*)pb)->file_path);
}



static PdfDocument*findPdfDocument(char*file_path)
{
PdfDocument*pdf_doc,tmp;
assert(file_path!=NULL);
if(PdfDocumentTree==NULL)
return NULL;
tmp.file_path= file_path;
pdf_doc= (PdfDocument*)avl_find(PdfDocumentTree,&tmp);
return pdf_doc;
}

#define PDF_CHECKSUM_SIZE 32

static char*get_file_checksum(char*a,file_error_mode fe)
{
struct stat finfo;
char*ck= NULL;
if(stat(a,&finfo)==0){
off_t size= finfo.st_size;
time_t mtime= finfo.st_mtime;
ck= (char*)malloc(PDF_CHECKSUM_SIZE);
if(ck==NULL)
pdftex_fail("PDF inclusion: out of memory while processing '%s'",
a);
snprintf(ck,PDF_CHECKSUM_SIZE,"%"PRIu64"_%"PRIu64,(uint64_t)size,
(uint64_t)mtime);
}else{
switch(fe){
case FE_FAIL:
pdftex_fail("PDF inclusion: could not stat() file '%s'",a);
break;
case FE_RETURN_NULL:
if(ck!=NULL)
free(ck);
ck= NULL;
break;
default:
assert(0);
}
}
return ck;
}





PdfDocument*refPdfDocument(char*file_path,file_error_mode fe)
{
char*checksum;
PdfDocument*pdf_doc;
PDFDoc*doc= NULL;
GooString*docName= NULL;
int new_flag= 0;
if((checksum= get_file_checksum(file_path,fe))==NULL){
assert(fe==FE_RETURN_NULL);
return(PdfDocument*)NULL;
}
assert(checksum!=NULL);
if((pdf_doc= findPdfDocument(file_path))==NULL){
#ifdef DEBUG
fprintf(stderr,"\nDEBUG: New PdfDocument %s\n",file_path);
#endif
new_flag= 1;
pdf_doc= new PdfDocument;
pdf_doc->file_path= xstrdup(file_path);
pdf_doc->checksum= checksum;
pdf_doc->doc= NULL;
pdf_doc->inObjList= NULL;
pdf_doc->ObjMapTree= NULL;
pdf_doc->occurences= 0;
pdf_doc->pc= 0;
}else{
#ifdef DEBUG
fprintf(stderr,"\nDEBUG: Found PdfDocument %s (%d)\n",
pdf_doc->file_path,pdf_doc->occurences);
#endif
assert(pdf_doc->checksum!=NULL);
if(strncmp(pdf_doc->checksum,checksum,PDF_CHECKSUM_SIZE)!=0){
pdftex_fail("PDF inclusion: file has changed '%s'",file_path);
}
free(checksum);
}
assert(pdf_doc!=NULL);
if(pdf_doc->doc==NULL){
#ifdef DEBUG
fprintf(stderr,"\nDEBUG: New PDFDoc %s (%d)\n",
pdf_doc->file_path,pdf_doc->occurences);
#endif
docName= new GooString(file_path);
doc= new PDFDoc(docName);
pdf_doc->pc++;

if(!doc->isOk()||!doc->okToPrint()){
switch(fe){
case FE_FAIL:
pdftex_fail("xpdf: reading PDF image failed");
break;
case FE_RETURN_NULL:
delete doc;

if(new_flag==1){
if(pdf_doc->file_path!=NULL)
free(pdf_doc->file_path);
if(pdf_doc->checksum!=NULL)
free(pdf_doc->checksum);
delete pdf_doc;
}
return(PdfDocument*)NULL;
break;
default:
assert(0);
}
}
pdf_doc->doc= doc;
}

if(PdfDocumentTree==NULL)
PdfDocumentTree= avl_create(CompPdfDocument,NULL,&avl_xallocator);
if((PdfDocument*)avl_find(PdfDocumentTree,pdf_doc)==NULL){
void**aa= avl_probe(PdfDocumentTree,pdf_doc);
assert(aa!=NULL);
}
pdf_doc->occurences++;
#ifdef DEBUG
fprintf(stderr,"\nDEBUG: Incrementing %s (%d)\n",
pdf_doc->file_path,pdf_doc->occurences);
#endif
return pdf_doc;
}






struct ObjMap{
Ref in;
int out_num;
};

static int CompObjMap(const void*pa,const void*pb,void*)
{
const Ref*a= &(((const ObjMap*)pa)->in);
const Ref*b= &(((const ObjMap*)pb)->in);
if(a->num> b->num)
return 1;
if(a->num<b->num)
return-1;
if(a->gen==b->gen)
return 0;
if(a->gen<b->gen)
return-1;
return 1;
}

static ObjMap*findObjMap(PdfDocument*pdf_doc,Ref in)
{
ObjMap*obj_map,tmp;
assert(pdf_doc!=NULL);
if(pdf_doc->ObjMapTree==NULL)
return NULL;
tmp.in= in;
obj_map= (ObjMap*)avl_find(pdf_doc->ObjMapTree,&tmp);
return obj_map;
}

static void addObjMap(PdfDocument*pdf_doc,Ref in,int out_num)
{
ObjMap*obj_map= NULL;
assert(findObjMap(pdf_doc,in)==NULL);
if(pdf_doc->ObjMapTree==NULL)
pdf_doc->ObjMapTree= avl_create(CompObjMap,NULL,&avl_xallocator);
obj_map= new ObjMap;
obj_map->in= in;
obj_map->out_num= out_num;
void**aa= avl_probe(pdf_doc->ObjMapTree,obj_map);
assert(aa!=NULL);
}










static int addInObj(PDF pdf,PdfDocument*pdf_doc,Ref ref)
{
ObjMap*obj_map;
InObj*p,*q,*n;
if(ref.num==0){
pdftex_fail("PDF inclusion: reference to invalid object"
" (is the included pdf broken?)");
}
if((obj_map= findObjMap(pdf_doc,ref))!=NULL)
return obj_map->out_num;
n= new InObj;
n->ref= ref;
n->next= NULL;
n->num= pdf_create_obj(pdf,obj_type_others,0);
addObjMap(pdf_doc,ref,n->num);
if(pdf_doc->inObjList==NULL)
pdf_doc->inObjList= n;
else{



for(p= pdf_doc->inObjList;p!=NULL;p= p->next)
q= p;
q->next= n;
}
return n->num;
}











static pdffloat conv_double_to_pdffloat(double n)
{
pdffloat a;
a.e= 6;
a.m= lround(n*ten_pow[a.e]);
return a;
}

static void copyObject(PDF,PdfDocument*,Object*);

void copyReal(PDF pdf,double d)
{
if(pdf->cave)
pdf_out(pdf,' ');
print_pdffloat(pdf,conv_double_to_pdffloat(d));
pdf->cave= true;
}

static void copyString(PDF pdf,GooString*string)
{
char*p;
unsigned char c;
size_t i,l;
p= string->getCString();
l= (size_t)string->getLength();
if(pdf->cave)
pdf_out(pdf,' ');
if(strlen(p)==l){
pdf_out(pdf,'(');
for(;*p!=0;p++){
c= (unsigned char)*p;
if(c=='('||c==')'||c=='\\')
pdf_printf(pdf,"\\%c",c);
else if(c<0x20||c> 0x7F)
pdf_printf(pdf,"\\%03o",(int)c);
else
pdf_out(pdf,c);
}
pdf_out(pdf,')');
}else{
pdf_out(pdf,'<');
for(i= 0;i<l;i++){
c= (unsigned char)string->getChar(i);
pdf_printf(pdf,"%.2x",(int)c);
}
pdf_out(pdf,'>');
}
pdf->cave= true;
}

static void copyName(PDF pdf,char*s)
{
pdf_out(pdf,'/');
for(;*s!=0;s++){
if(isdigit(*s)||isupper(*s)||islower(*s)||*s=='_'||
*s=='.'||*s=='-'||*s=='+')
pdf_out(pdf,*s);
else
pdf_printf(pdf,"#%.2X",*s&0xFF);
}
pdf->cave= true;
}

static void copyArray(PDF pdf,PdfDocument*pdf_doc,Array*array)
{
int i,l;
Object obj1;
pdf_begin_array(pdf);
for(i= 0,l= array->getLength();i<l;++i){
array->getNF(i,&obj1);
copyObject(pdf,pdf_doc,&obj1);
obj1.free();
}
pdf_end_array(pdf);
}

static void copyDict(PDF pdf,PdfDocument*pdf_doc,Dict*dict)
{
int i,l;
Object obj1;
pdf_begin_dict(pdf);
for(i= 0,l= dict->getLength();i<l;++i){
copyName(pdf,dict->getKey(i));
dict->getValNF(i,&obj1);
copyObject(pdf,pdf_doc,&obj1);
obj1.free();
}
pdf_end_dict(pdf);
}

static void copyStreamStream(PDF pdf,Stream*str)
{
int c,i,len= 1024;
str->reset();
i= len;
while((c= str->getChar())!=EOF){
if(i==len){
pdf_room(pdf,len);
i= 0;
}
pdf_quick_out(pdf,c);
i++;
}
}

static void copyStream(PDF pdf,PdfDocument*pdf_doc,Stream*stream)
{
copyDict(pdf,pdf_doc,stream->getDict());
pdf_begin_stream(pdf);
assert(pdf->zip_write_state==NO_ZIP);
copyStreamStream(pdf,stream->getUndecodedStream());
pdf_end_stream(pdf);
}

static void copyObject(PDF pdf,PdfDocument*pdf_doc,Object*obj)
{
switch(obj->getType()){
case objBool:
pdf_add_bool(pdf,(int)obj->getBool());
break;
case objInt:
pdf_add_int(pdf,obj->getInt());
break;
case objReal:
copyReal(pdf,obj->getReal());
break;



case objString:
copyString(pdf,obj->getString());
break;
case objName:
copyName(pdf,obj->getName());
break;
case objNull:
pdf_add_null(pdf);
break;
case objArray:
copyArray(pdf,pdf_doc,obj->getArray());
break;
case objDict:
copyDict(pdf,pdf_doc,obj->getDict());
break;
case objStream:
copyStream(pdf,pdf_doc,obj->getStream());
break;
case objRef:
pdf_add_ref(pdf,addInObj(pdf,pdf_doc,obj->getRef()));
break;
case objCmd:
case objError:
case objEOF:
case objNone:
pdftex_fail("PDF inclusion: type <%s> cannot be copied",
obj->getTypeName());
break;
default:
assert(0);
}
}



static void writeRefs(PDF pdf,PdfDocument*pdf_doc)
{
InObj*r,*n;
Object obj1;
XRef*xref;
PDFDoc*doc= pdf_doc->doc;
xref= doc->getXRef();
for(r= pdf_doc->inObjList;r!=NULL;){
xref->fetch(r->ref.num,r->ref.gen,&obj1);
if(obj1.isStream())
pdf_begin_obj(pdf,r->num,OBJSTM_NEVER);
else
pdf_begin_obj(pdf,r->num,2);
copyObject(pdf,pdf_doc,&obj1);
obj1.free();
pdf_end_obj(pdf);
n= r->next;
delete r;
pdf_doc->inObjList= r= n;
}
}



static PDFRectangle*get_pagebox(Page*page,int pagebox_spec)
{
switch(pagebox_spec){
case PDF_BOX_SPEC_MEDIA:
return page->getMediaBox();
break;
case PDF_BOX_SPEC_CROP:
return page->getCropBox();
break;
case PDF_BOX_SPEC_BLEED:
return page->getBleedBox();
break;
case PDF_BOX_SPEC_TRIM:
return page->getTrimBox();
break;
case PDF_BOX_SPEC_ART:
return page->getArtBox();
break;
default:
pdftex_fail("PDF inclusion: unknown value of pagebox spec (%i)",
(int)pagebox_spec);
}
return page->getMediaBox();
}







void
read_pdf_info(image_dict*idict,int minor_pdf_version_wanted,
int pdf_inclusion_errorlevel,img_readtype_e readtype)
{
PdfDocument*pdf_doc;
PDFDoc*doc;
Catalog*catalog;
Page*page;
int rotate;
PDFRectangle*pagebox;
int pdf_major_version_found,pdf_minor_version_found;
float xsize,ysize,xorig,yorig;
assert(idict!=NULL);
assert(img_type(idict)==IMG_TYPE_PDF);
assert(readtype==IMG_CLOSEINBETWEEN);

if(isInit==gFalse){
globalParams= new GlobalParams();
globalParams->setErrQuiet(gFalse);
isInit= gTrue;
}

pdf_doc= refPdfDocument(img_filepath(idict),FE_FAIL);
doc= pdf_doc->doc;
catalog= doc->getCatalog();




pdf_major_version_found= doc->getPDFMajorVersion();
pdf_minor_version_found= doc->getPDFMinorVersion();
if((pdf_major_version_found> 1)
||(pdf_minor_version_found> minor_pdf_version_wanted)){
const char*msg= 
"PDF inclusion: found PDF version <%d.%d>, but at most version <1.%d> allowed";
if(pdf_inclusion_errorlevel> 0){
pdftex_fail(msg,pdf_major_version_found,pdf_minor_version_found,
minor_pdf_version_wanted);
}else{
pdftex_warn(msg,pdf_major_version_found,pdf_minor_version_found,
minor_pdf_version_wanted);
}
}
img_totalpages(idict)= catalog->getNumPages();
if(img_pagename(idict)){

GooString name(img_pagename(idict));
LinkDest*link= doc->findDest(&name);
if(link==NULL||!link->isOk())
pdftex_fail("PDF inclusion: invalid destination <%s>",
img_pagename(idict));
Ref ref= link->getPageRef();
img_pagenum(idict)= catalog->findPage(ref.num,ref.gen);
if(img_pagenum(idict)==0)
pdftex_fail("PDF inclusion: destination is not a page <%s>",
img_pagename(idict));
delete link;
}else{

if(img_pagenum(idict)<=0
||img_pagenum(idict)> img_totalpages(idict))
pdftex_fail("PDF inclusion: required page <%i> does not exist",
(int)img_pagenum(idict));
}

page= catalog->getPage(img_pagenum(idict));


pagebox= get_pagebox(page,img_pagebox(idict));
if(pagebox->x2> pagebox->x1){
xorig= pagebox->x1;
xsize= pagebox->x2-pagebox->x1;
}else{
xorig= pagebox->x2;
xsize= pagebox->x1-pagebox->x2;
}
if(pagebox->y2> pagebox->y1){
yorig= pagebox->y1;
ysize= pagebox->y2-pagebox->y1;
}else{
yorig= pagebox->y2;
ysize= pagebox->y1-pagebox->y2;
}

img_xsize(idict)= bp2sp(xsize);
img_ysize(idict)= bp2sp(ysize);
img_xorig(idict)= bp2sp(xorig);
img_yorig(idict)= bp2sp(yorig);



rotate= page->getRotate();
switch(((rotate%360)+360)%360){
case 0:
img_rotation(idict)= 0;
break;
case 90:
img_rotation(idict)= 3;
break;
case 180:
img_rotation(idict)= 2;
break;
case 270:
img_rotation(idict)= 1;
break;
default:
pdftex_warn
("PDF inclusion: "
"/Rotate parameter in PDF file not multiple of 90 degrees.");
}


if(page->getGroup()!=NULL)
img_set_group(idict);

if(readtype==IMG_CLOSEINBETWEEN)
unrefPdfDocument(img_filepath(idict));
}






void write_epdf(PDF pdf,image_dict*idict)
{
PdfDocument*pdf_doc;
PDFDoc*doc;
Catalog*catalog;
Page*page;
Ref*pageref;
Dict*pageDict;
Object obj1,contents,pageobj,pagesobj1,pagesobj2,*op1,*op2,*optmp;
PDFRectangle*pagebox;
int i,l;
double bbox[4];
char s[256];
const char*pagedictkeys[]= 
{"Group","LastModified","Metadata","PieceInfo","Resources",
"SeparationInfo",NULL
};
assert(idict!=NULL);


pdf_doc= refPdfDocument(img_filepath(idict),FE_FAIL);
doc= pdf_doc->doc;
catalog= doc->getCatalog();
page= catalog->getPage(img_pagenum(idict));
pageref= catalog->getPageRef(img_pagenum(idict));
assert(pageref!=NULL);
doc->getXRef()->fetch(pageref->num,pageref->gen,&pageobj);
pageDict= pageobj.getDict();


pdf_begin_obj(pdf,img_objnum(idict),OBJSTM_NEVER);
pdf_begin_dict(pdf);
pdf_dict_add_name(pdf,"Type","XObject");
pdf_dict_add_name(pdf,"Subtype","Form");

if(img_attr(idict)!=NULL&&strlen(img_attr(idict))> 0)
pdf_printf(pdf,"\n%s\n",img_attr(idict));
pdf_dict_add_int(pdf,"FormType",1);


pdf_dict_add_img_filename(pdf,idict);
snprintf(s,30,"%s.PageNumber",pdfkeyprefix);
pdf_dict_add_int(pdf,s,(int)img_pagenum(idict));
doc->getDocInfoNF(&obj1);
if(obj1.isRef()){

snprintf(s,30,"%s.InfoDict",pdfkeyprefix);
pdf_dict_add_ref(pdf,s,addInObj(pdf,pdf_doc,obj1.getRef()));
}
obj1.free();
if(img_is_bbox(idict)){
bbox[0]= sp2bp(img_bbox(idict)[0]);
bbox[1]= sp2bp(img_bbox(idict)[1]);
bbox[2]= sp2bp(img_bbox(idict)[2]);
bbox[3]= sp2bp(img_bbox(idict)[3]);
}else{

pagebox= get_pagebox(page,img_pagebox(idict));
bbox[0]= pagebox->x1;
bbox[1]= pagebox->y1;
bbox[2]= pagebox->x2;
bbox[3]= pagebox->y2;
}
pdf_add_name(pdf,"BBox");
pdf_begin_array(pdf);
copyReal(pdf,bbox[0]);
copyReal(pdf,bbox[1]);
copyReal(pdf,bbox[2]);
copyReal(pdf,bbox[3]);
pdf_end_array(pdf);





pageDict->lookupNF((char*)"Metadata",&obj1);
if(!obj1.isNull()&&!obj1.isRef())
pdftex_warn("PDF inclusion: /Metadata must be indirect object");
obj1.free();


for(i= 0;pagedictkeys[i]!=NULL;i++){
pageDict->lookupNF((char*)pagedictkeys[i],&obj1);
if(!obj1.isNull()){
pdf_add_name(pdf,pagedictkeys[i]);
copyObject(pdf,pdf_doc,&obj1);
}
obj1.free();
}





pageDict->lookupNF((char*)"Resources",&obj1);
if(obj1.isNull()){
op1= &pagesobj1;
op2= &pagesobj2;
pageDict->lookup((char*)"Parent",op1);
while(op1->isDict()){
obj1.free();
op1->dictLookupNF((char*)"Resources",&obj1);
if(!obj1.isNull()){
pdf_add_name(pdf,(const char*)"Resources");
copyObject(pdf,pdf_doc,&obj1);
break;
}
op1->dictLookup((char*)"Parent",op2);
optmp= op1;
op1= op2;
op2= optmp;
op2->free();
};
if(!op1->isDict())
pdftex_warn("PDF inclusion: Page /Resources missing.");
op1->free();
}
obj1.free();


page->getContents(&contents);
if(contents.isStream()){









contents.streamGetDict()->lookup((char*)"F",&obj1);
if(!obj1.isNull()){
pdftex_fail("PDF inclusion: Unsupported external stream");
}
obj1.free();
contents.streamGetDict()->lookup((char*)"Length",&obj1);
assert(!obj1.isNull());
pdf_add_name(pdf,(const char*)"Length");
copyObject(pdf,pdf_doc,&obj1);
obj1.free();
contents.streamGetDict()->lookup((char*)"Filter",&obj1);
if(!obj1.isNull()){
pdf_add_name(pdf,(const char*)"Filter");
copyObject(pdf,pdf_doc,&obj1);
obj1.free();
contents.streamGetDict()->lookup((char*)"DecodeParms",&obj1);
if(!obj1.isNull()){
pdf_add_name(pdf,(const char*)"DecodeParms");
copyObject(pdf,pdf_doc,&obj1);
}
}
obj1.free();
pdf_end_dict(pdf);
pdf_begin_stream(pdf);
copyStreamStream(pdf,contents.getStream()->getBaseStream());
pdf_end_stream(pdf);
pdf_end_obj(pdf);
}else if(contents.isArray()){
pdf_dict_add_streaminfo(pdf);
pdf_end_dict(pdf);
pdf_begin_stream(pdf);
for(i= 0,l= contents.arrayGetLength();i<l;++i){
copyStreamStream(pdf,(contents.arrayGet(i,&obj1))->getStream());
obj1.free();
if(i<(l-1)){


pdf_out(pdf,' ');
}
}
pdf_end_stream(pdf);
pdf_end_obj(pdf);
}else{
pdf_dict_add_streaminfo(pdf);
pdf_end_dict(pdf);
pdf_begin_stream(pdf);
pdf_end_stream(pdf);
pdf_end_obj(pdf);
}

writeRefs(pdf,pdf_doc);
contents.free();
pageobj.free();


#if 0
unrefPdfDocument(img_filepath(idict));
#endif
}




static void deletePdfDocumentPdfDoc(PdfDocument*pdf_doc)
{
InObj*r,*n;
assert(pdf_doc!=NULL);

for(r= pdf_doc->inObjList;r!=NULL;r= n){
n= r->next;
delete r;
}
#ifdef DEBUG
fprintf(stderr,"\nDEBUG: Deleting PDFDoc %s\n",pdf_doc->file_path);
#endif
delete pdf_doc->doc;
pdf_doc->doc= NULL;
pdf_doc->pc++;
}

static void destroyPdfDocument(void*pa,void*)
{
PdfDocument*pdf_doc= (PdfDocument*)pa;
deletePdfDocumentPdfDoc(pdf_doc);

}




void unrefPdfDocument(char*file_path)
{
PdfDocument*pdf_doc= findPdfDocument(file_path);
assert(pdf_doc!=NULL);
assert(pdf_doc->occurences!=0);
pdf_doc->occurences--;
#ifdef DEBUG
fprintf(stderr,"\nDEBUG: Decrementing %s (%d)\n",
pdf_doc->file_path,pdf_doc->occurences);
#endif
if(pdf_doc->occurences==0){
assert(pdf_doc->inObjList==NULL);
deletePdfDocumentPdfDoc(pdf_doc);
}
}




void epdf_free()
{
if(PdfDocumentTree!=NULL)
avl_destroy(PdfDocumentTree,destroyPdfDocument);
PdfDocumentTree= NULL;
if(isInit==gTrue)
delete globalParams;
isInit= gFalse;
}/*:1*/
